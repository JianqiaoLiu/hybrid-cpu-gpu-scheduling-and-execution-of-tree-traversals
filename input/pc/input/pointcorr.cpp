/*
SIMTree, a framework to automatically vectorize tree traversal algorithms.
Youngjoon Jo (yjo@purdue.edu)

Copyright 2013, School of Electrical and Computer Engineering, Purdue University.
 */

#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <float.h>

#include <iostream>
using namespace std;

const int MAX_POINTS_IN_CELL = 4;
#define DIM 7

#define TRACK_TRAVERSALS

class Point {
public:
	Point();
	~Point();

	float coord[DIM];
	int corr;
#ifdef TRACK_TRAVERSALS
	int num_nodes_traversed;
	int id;
private:
	static int next_id;
#endif
};

class Node {
public:
	Node();
	~Node();

	int axis;
	float splitval;
	float min[DIM];
	float max[DIM];
	Point* points[MAX_POINTS_IN_CELL];
	Node *left;
	Node *right;
#ifdef TRACK_TRAVERSALS
	int id;
private:
	static int next_id;
#endif
};


#ifdef TRACK_TRAVERSALS
int Point::next_id = 0;
int Node::next_id = 0;
#endif

Point::Point() {
	corr = 0;
#ifdef TRACK_TRAVERSALS
	num_nodes_traversed = 0;
	id = next_id++;
#endif
}

Point::~Point() {
}

Node::Node() {
	for (int i = 0; i < DIM; i++) {
		min[i] = FLT_MAX;
		max[i] = 0;
	}
	for (int i = 0; i < MAX_POINTS_IN_CELL; i++) {
		points[i] = NULL;
	}
	left = NULL;
	right = NULL;
#ifdef TRACK_TRAVERSALS
	id = next_id++;
#endif
}

Node::~Node() {
}

#pragma afterClassDecs

Point *points;
Point **ppoints;
Node *root;
int sort_split;
float rad;

int npoints;


void read_input(int argc, char **argv);
void read_point(FILE *in, Point *p);
void correlation_search(Point *point, Node *root);
bool can_correlate(Point * point, Node * cell);


Node * construct_tree(Point *points, int start_idx, int end_idx, int depth);
void sort_points(Point *points, int start_idx, int end_idx, int depth);
int compare_point(const void *a, const void *b);

inline float distance_axis(Point *a, Point *b, int axis) {
	return (a->coord[axis] - b->coord[axis]) * (a->coord[axis] - b->coord[axis]);
}

inline float distance(Point *a, Point *b) {
	float d = 0;
	for(int i = 0; i < DIM; i++) {
		d += distance_axis(a,b,i);
	}
	return d;
}


void free_tree(Node *n)
{
	if (n->left != NULL) free_tree(n->left);
	if (n->right != NULL) free_tree(n->right);
	delete n;
}


void correlation_search(Point *point, Node *node) {
	assert(node != NULL);

#ifdef TRACK_TRAVERSALS
	point->num_nodes_traversed++;
	//if (point->id == 10000) cout << node->id << " " << point->closest_dist << endl;
#endif

	// is this node closer than the current best?
	if(!can_correlate(point, node)) {
		return;
	}

	if(node->axis == DIM) {
		for (int i = 0; i < MAX_POINTS_IN_CELL; i++) {
			Point *candidate = node->points[i];
			if (candidate == NULL) break;
			float dist = distance(point, candidate);
			if (sqrt(dist) < rad) point->corr++;
		}
	} else {
		correlation_search(point, node->left);
		correlation_search(point, node->right);
	}
}


void find_correlation(Point *point, Node *node) {
	correlation_search(point, node);
}


bool can_correlate(Point * point, Node * cell) {
	float sum = 0.0;
	float boxsum = 0.0;
	for(int i = 0; i < DIM; i++) {
		float center = (cell->max[i] + cell->min[i]) / 2;
		float boxdist = (cell->max[i] - cell->min[i]) / 2;
		float dist = point->coord[i] - center;
		sum += dist * dist;
		boxsum += boxdist * boxdist;
	}
	if(sqrt(sum) - sqrt(boxsum) < rad)
		return true;
	else
		return false;
}

int compare_point(const void *a, const void *b) {
	if(((Point *)a)->coord[sort_split] < ((Point *)b)->coord[sort_split]) {
		return -1;
	} else if(((Point *)a)->coord[sort_split] > ((Point *)b)->coord[sort_split]) {
		return 1;
	} else {
		return 0;
	}
}

float max(float a, float b) {
	return a > b ? a : b;
}

float min(float a, float b) {
	return a < b ? a : b;
}

Node * construct_tree(Point * points, int lb, int ub, int depth) {
	Node *node = new Node;
	int size = ub - lb + 1;
	int mid;
	int i, j;

	if (size <= MAX_POINTS_IN_CELL) {
		for (i = 0; i < size; i++) {
			node->points[i] = &points[lb + i];
			for (j = 0; j < DIM; j++) {
				node->max[j] = max(node->max[j], points[lb + i].coord[j]);
				node->min[j] = min(node->min[j], points[lb + i].coord[j]);
				//printf("%f %f %f\n", node->max[j], node->min[j], points[lb + i].coord[j]);

			}
			//exit(0);
		}
		node->axis = DIM; // leaf node has axis of DIM
		return node;

	} else {
		sort_split = depth % DIM;
		qsort(&points[lb], ub - lb + 1, sizeof(Point), compare_point);
		mid = (ub + lb) / 2;

		node->axis = depth % DIM;
		node->splitval = points[mid].coord[node->axis];
		node->left = construct_tree(points, lb, mid, depth + 1);
		node->right = construct_tree(points, mid+1, ub, depth + 1);

		for(j = 0; j < DIM; j++) {
			node->min[j] = min(node->left->min[j], node->right->min[j]);
			node->max[j] = max(node->left->max[j], node->right->max[j]);
			//printf("%f %f %f\n", node->max[j], node->min[j], node->left->min[j]);
		}
		return node;
	}	
}

void sort_points(Point * points, int lb, int ub, int depth) {
	int mid;
	if(lb >= ub)
		return;

	sort_split = depth % DIM;
	qsort(&points[lb], ub - lb + 1, sizeof(Point), compare_point);
	mid = (ub + lb) / 2;

	if(mid > lb) {
		sort_points(points, lb, mid - 1, depth + 1);
		sort_points(points, mid, ub, depth + 1);
	} else {
		sort_points(points, lb, mid, depth + 1);
		sort_points(points, mid+1, ub, depth + 1);
	}
}

void read_input(int argc, char **argv) {
	FILE *in;

	if(argc != 5 && argc != 4) {
		fprintf(stderr, "usage: pointcorr <DIM> <rad> <npoints> [input_file]\n");
		exit(1);
	}

	//DIM = atoi(argv[0]);
	if(DIM <= 0) {
		fprintf(stderr, "Invalid DIM\n");
		exit(1);
	}

	rad = atof(argv[2]);

	npoints = atol(argv[3]);
	if(npoints <= 0) {
		fprintf(stderr, "Not enough points.\n");
		exit(1);
	}

	points = new Point[npoints];

	if(argc == 5) {
		in = fopen(argv[4], "r");
		if(in == NULL) {
			fprintf(stderr, "Could not open %s\n", argv[4]);
			exit(1);
		}

		for(int i = 0; i < npoints; i++) {
			read_point(in, &points[i]);
		}
		fclose(in);
	} else {
		srand(0);
		for(int i = 0; i < npoints; i++) {
			for(int j = 0; j < DIM; j++) {
				points[i].coord[j] = (float)rand() / RAND_MAX;
			}
            printf("%f %f %f %f %f %f %f\n", points[i].coord[0], points[i].coord[1], points[i].coord[2], points[i].coord[3], points[i].coord[4], points[i].coord[5], points[i].coord[6]);
		}
	}
}

void read_point(FILE *in, Point *p) {
	int dummy;
	if(fscanf(in, "%d", &dummy) != 1) {
		fprintf(stderr, "Input file not large enough.\n");
		exit(1);
	}
	for(int j = 0; j < DIM; j++) {
		if(fscanf(in, "%f", &p->coord[j]) != 1) {
			fprintf(stderr, "Input file not large enough.\n");
			exit(1);
		}
	}
}

int main(int argc, char **argv) {

	read_input(argc, argv);
	cout << "rad " << rad << endl;

	ppoints = new Point*[npoints];
	for (int i = 0; i < npoints; i++) {
		ppoints[i] = &points[i];
	}

	root = construct_tree(points, 0, npoints - 1, 0);

//	if(!Harness::get_sort_flag()) {
		// randomize points
		srand(0);
		for (int i = 0; i < npoints; i++) {
			int r = rand() % npoints;
			Point *temp = ppoints[i];
			ppoints[i] = ppoints[r];
			ppoints[r] = temp;
		}
//	}
#pragma parallelForOnTree
		for(int i = 0; i < npoints; i++) {
			Point *p = ppoints[i];
			find_correlation(p, root);
		}


	long long sum = 0;
	for (int i = 0; i < npoints; i++) {
		sum += points[i].corr;
	}
	cout << "avg corr: " << (float)sum / npoints << endl;

#ifdef TRACK_TRAVERSALS
	int sum_nodes_traversed = 0;
	for (int i = 0; i < npoints; i++) {
		Point *p = &points[i];
		sum_nodes_traversed += p->num_nodes_traversed;
		//printf("%d %d\n", p->id, p->num_nodes_traversed);
	}
	printf("sum_nodes_traversed:%d\n", sum_nodes_traversed);
#endif

	delete [] ppoints;
	delete [] points;
	free_tree(root);

	return 0;
}

