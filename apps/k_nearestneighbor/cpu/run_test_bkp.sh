#! /bin/sh -x

INPUT_SIZE=200000
INPUT_DIR=$SCRATCH/Common/input/pointcorr
#INPUT_DIM=(7 7 7 2)
INPUT_DIM=(2)
#INPUT_FILES=($INPUT_DIR/covtype.7d $INPUT_DIR/mnist.7d $INPUT_DIR/random.7d $INPUT_DIR/geocity.txt)
INPUT_FILES=($INPUT_DIR/geocity.txt)

NUM_RUNS=10
THREADS=(32 24 20 16 12 8 4 2 1)

MAKE_ARGS=()

# when tracking traversals only do a single run
# these numbers never change
for arg in $*
do
		MAKE_ARGS[${#MAKE_ARGS}]=$arg
		if [[ $arg = "TRACK_TRAVERSALS=1" ]]
		then
		 	THREADS=(32)
			NUM_RUNS=1		
		fi
done

host=$(hostname)
start=$(date)

(( i=0 ))
for file in ${INPUT_FILES[*]}
do
		make clean;
		make ${MAKE_ARGS[*]} DIM=${INPUT_DIM[$i]}

		for ((i = 0; i < NUM_RUNS; i++))
		do
				f=$(basename $file)
				for t in ${THREADS[*]}
				do
						./nn -s -t $t 8  $file ${INPUT_SIZE} 2>&1 | collect.py ${f}_$host_${t}_sorted.stats 2>&1 >>${f}_$host_${t}_sorted.out
						./nn -t $t 8  $file ${INPUT_SIZE} 2>&1 | collect.py ${f}_$host_${t}.stats 2>&1 >>${f}_$host_${t}.out
				done

				now=$(date)
				echo -e "Started: $start\nNow: $now" | mail -s "KNN Benchmark $i/${NUM_RUNS} Completed (CPU ${f}) - $host" "mgoldfar@purdue.edu"
	 done
	 (( i++ ))
done

exit 0;
