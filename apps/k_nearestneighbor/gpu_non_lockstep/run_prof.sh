#make clean;
#make DIM=7 SPLICE_DEPTH=8

#./knn 8 ../../../input/pc/covtype.7d 200000
#./knn 8 ../../../input/pc/mnist.7d 200000
#./knn 8 ../../../input/pc/random.7d 200000

make clean;
make DIM=3 SPLICE_DEPTH=8;
./nn 8 ../../../input/pc/geocity.txt 200000
./nn -s 8 ../../../input/pc/geocity.txt 200000

make clean;
make DIM=2 SPLICE_DEPTH=8;
./nn 8 ../../../input/pc/geocity.txt 200000
./nn -s 8 ../../../input/pc/geocity.txt 200000
