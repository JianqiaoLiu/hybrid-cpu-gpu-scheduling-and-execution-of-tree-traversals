/* -*- mode: c -*- */

#include <float.h>
#include "nn.h"
#include "nn_gpu.h"
#include <stdio.h>

__global__ void init_kernel(void) {

}

__global__ void nearest_neighbor_search (gpu_tree gpu_tree, int nsearchpoints, node *d_search_points, float *d_nearest_distance, int *d_nearest_point_index, int K
#ifdef TRACK_TRAVERSALS
		, int *d_nodes_accessed
#endif
)
{
	float search_points[DIM];
	float nearest_distance[8];
	int nearest_point_index[8];
	float stk_axis_dist[64];

	int pidx;
	int i, j;

	__shared__ gpu_tree_node_0 cur_node0[NUM_WARPS_PER_BLOCK];
	__shared__ gpu_tree_node_1 cur_node1[NUM_WARPS_PER_BLOCK];
	__shared__ gpu_tree_node_2 cur_node2[NUM_WARPS_PER_BLOCK];
	__shared__ int stk_node[NUM_WARPS_PER_BLOCK][64];
//	__shared__ float stk_axis_dist[NUM_WARPS_PER_BLOCK][64];

	bool curr, cond, status;
    bool opt1, opt2;
	unsigned int critical;
	__shared__ unsigned int vote_left;
	__shared__ unsigned int vote_right;
	__shared__ unsigned int num_left;
	__shared__ unsigned int num_right;

	int cur_node_index;
	__shared__ unsigned int SP[NUM_WARPS_PER_BLOCK];

	int current_split;
	float axis_dist;
	float dist;
	float t;

	float tmpdist;
	int tmpidx;
	int n;

	#ifdef TRACK_TRAVERSALS
	int nodes_accessed;
	#endif

#include "nn_kernel_macros.inc"

	for (pidx = blockIdx.x * blockDim.x + threadIdx.x; pidx < nsearchpoints; pidx += blockDim.x * gridDim.x) {
//	for (pidx = blockIdx.x * blockDim.x + threadIdx.x; pidx < 32; pidx += blockDim.x * gridDim.x) {

		for (j = 0; j < DIM; j++) {
			search_points[j] = d_search_points[pidx].point[j];
		}

		for(i = 0; i < K; i++) {
			nearest_point_index[i] = -1;
			nearest_distance[i] = FLT_MAX;
		}

		#ifdef TRACK_TRAVERSALS
		nodes_accessed = 0;
		#endif

		// run this for some number of iterations until done...
		STACK_INIT();
		status = 1;
		critical = 63;
		cond = 1;
		STACK_NODE=0;
		STACK_AXIS_DIST = FLT_MIN;
		while(sp >= 1) {
			// get top of stack
			cur_node_index = STACK_NODE;
			axis_dist = STACK_AXIS_DIST;

			if (status == 0 && critical >= sp) {
				status = 1;
			}
			if (status) {
				cond = (axis_dist <= nearest_distance[0]);
			} else {
//				cond = 0;
			}

			STACK_POP();

			if (!__any(cond)) {
				continue;
			}

/*			if (pidx == 0) {
				printf("%dth point with index = %d. status = %d, critical = %d, cond = %d, sp = %d, axis_dist = %f, nearest = %f\n", cur_node_index, POINT_INDEX, status, critical, cond, sp + 1, axis_dist, nearest_distance[0]);
			}*/

			// notice the mask stk push and pop
//			if (status == 0 && critical >= sp) {
//				status = 1;
//			}

			if (status) {
				if (!cond) {
					status = 0;
					critical = sp;
				} else {
					#ifdef TRACK_TRAVERSALS
					nodes_accessed++;
					#endif

					cur_node0[WARP_INDEX] = gpu_tree.nodes0[cur_node_index];
					current_split = AXIS;
					cur_node1[WARP_INDEX] = gpu_tree.nodes1[cur_node_index];
					cur_node2[WARP_INDEX] = gpu_tree.nodes2[cur_node_index];

					dist = 0.0;
					for (i = 0; i < DIM; i++) {
						t = (POINT[i] - search_points[i]);
						dist +=  t*t;
					}

					// update closest point:
					if(dist < nearest_distance[0]) {
						nearest_distance[0] = dist;
						nearest_point_index[0] = POINT_INDEX;

						// push the value back to maintain sorted order
						for(n = 0; n < K-1 && nearest_distance[n] < nearest_distance[n+1]; n++) {
							tmpdist = nearest_distance[n];
							tmpidx = nearest_point_index[n];
							nearest_distance[n] = nearest_distance[n+1];
							nearest_point_index[n] = nearest_point_index[n+1];
							nearest_distance[n+1] = tmpdist;
							nearest_point_index[n+1] = tmpidx;
						}
					}
					axis_dist =	(search_points[current_split] - POINT_SPLIT (current_split));

					opt1 = search_points[current_split] <= POINT_SPLIT(current_split);
					opt2 = search_points[current_split] > POINT_SPLIT(current_split);
					vote_left = __ballot(opt1);
					vote_right = __ballot(opt2);
					num_left = __popc(vote_left);
					num_right = __popc(vote_right);

//					if (pidx == 0) {
//						printf("Thread 0 joined the ballot for %dth point with index = %d. left = %d, right = %d\n", cur_node_index, POINT_INDEX, num_left, num_right);
//					}

					if ((num_left > num_right) && LEFT != NULL_NODE) {
						if(RIGHT != NULL_NODE) {
							STACK_PUSH();
							STACK_NODE = RIGHT;
							if(opt1)
								STACK_AXIS_DIST = axis_dist * axis_dist;
							else
								STACK_AXIS_DIST = FLT_MIN;

/*							if (pidx == 0) {
								printf("	axis_dist = %f, STACK_AXIS_DIST = %f, num_left = %d, num_right = %d, vote_right = %d\n", axis_dist, STACK_AXIS_DIST, num_left, num_right, vote_right);
							}*/
						}

						STACK_PUSH();
						STACK_NODE = LEFT;
						STACK_AXIS_DIST = FLT_MIN;

					} else if (RIGHT != NULL_NODE) {
						if(LEFT != NULL_NODE) {
							STACK_PUSH();
							STACK_NODE = LEFT;
							if(opt2)
								STACK_AXIS_DIST = axis_dist * axis_dist;
							else
								STACK_AXIS_DIST = FLT_MIN;
						}

/*						if (pidx == 0) {
							printf("	axis_dist = %f, STACK_AXIS_DIST = %f, num_left = %d, num_right = %d, vote_left = %d\n", axis_dist, STACK_AXIS_DIST, num_left, num_right, vote_left);
						}*/

						STACK_PUSH();
						STACK_NODE = RIGHT;
						STACK_AXIS_DIST = FLT_MIN;
					}
				}
			}


		}

		// Save to global memory
		for(i = 0; i < K; i++) {
			d_nearest_point_index[K*pidx+i] = nearest_point_index[i];
			d_nearest_distance[K*pidx+i] = nearest_distance[i];
		}
		#ifdef TRACK_TRAVERSALS
		d_nodes_accessed[pidx] = nodes_accessed;
		#endif
    }
}

__device__ float
distance (float *a, float *b)
{
  int i;
  float d = 0;
  // returns distance squared
#pragma unroll
  for (i = 0; i < DIM; i++)
    {
      d += distance_axis (a, b, i);
    }

  return d;
}
