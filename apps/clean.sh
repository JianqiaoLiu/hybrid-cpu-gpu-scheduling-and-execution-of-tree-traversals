#! /bin/sh

for dir in *
do
		if [[ -d $dir ]] && [[ -f ./$dir/clean.sh ]]
		then				
				cd $dir
				echo "=>>> Cleaning $dir <<<="
				./clean.sh
				cd ..
		fi
done

exit 0
