/* -*- mode: C++ -*- */

#include "vptree.h"

__global__ void init_kernel(void) {
	return;
}


__global__ void search_kernel(struct __GPU_tree d_tree, struct Point *__GPU_point_Point_d, struct __GPU_point *__GPU_point_array_d, struct Point *__GPU_node_point_d) {
	
	int pidx;
	//struct Point *target;

	int sp;
	int cur_node_index;
	float dist;

	__shared__ int node_stack[NWARPS_PER_BLOCK][128];

	bool stk_mask[128];
	bool curr_mask = true;

	struct __GPU_Node node;
	struct __GPU_Node parent_node; // structs cached into registers

	__shared__ struct Point target[THREADS_PER_BLOCK]; // point data cached in SMEM

	for (pidx = blockIdx.x * blockDim.x + threadIdx.x; pidx < d_tree.npoints; pidx += blockDim.x * gridDim.x) {
		
		target[threadIdx.x] = __GPU_point_Point_d[__GPU_point_array_d[pidx].target];
		sp = 0;
		node_stack[WARP_INDEX][0] = 0;
		stk_mask[sp] = true;
		
		while(sp >= 0) {
			curr_mask = stk_mask[sp];
			cur_node_index = node_stack[WARP_INDEX][sp];
            sp --;
			if(cur_node_index == -1) {
				continue;
			}

			node = d_tree.nodes[cur_node_index];
            dist = 0.0;
            bool cond = true;
			if (curr_mask) 
            {
#ifdef TRACK_TRAVERSALS
				target[threadIdx.x].num_nodes_traversed++;
#endif

				int parent_node_index = node.parent;
		
				if(parent_node_index != -1) {
					parent_node = d_tree.nodes[parent_node_index];
					float upperDist = 0.0;
					int i;
					struct Point *a = &__GPU_node_point_d[parent_node.point];
					for(i = 0; i < DIM; i++) {
						float diff = (a->coord[i] - target[threadIdx.x].coord[i]);
						upperDist += (diff*diff);
					}
					upperDist = sqrt(upperDist);

					if(parent_node.right == cur_node_index) {
						cond = (upperDist + target[threadIdx.x].tau) >= parent_node.threshold;
					} else if(parent_node.left == cur_node_index) {
						cond = (upperDist - target[threadIdx.x].tau) <= parent_node.threshold;
					}
				}
            }
            curr_mask = curr_mask && cond; 
            if(!__any(curr_mask)) {
#ifdef TRACK_TRAVERSALS
                target[threadIdx.x].num_trunc++;
#endif
                continue;
            }  

            if (curr_mask) {
		        int i;
				struct Point *a = &__GPU_node_point_d[node.point];
				for(i = 0; i < DIM; i++) {
					float diff = (a->coord[i] - target[threadIdx.x].coord[i]);
				    dist += diff * diff;
			    }
			    dist = sqrt(dist);

			    if(dist < target[threadIdx.x].tau) {
				    target[threadIdx.x].closest_label = __GPU_node_point_d[node.point].label;
				    target[threadIdx.x].tau = dist;
			    }
			}

			int left = node.left; // cache to registers (CSE)
			int right = node.right;
			if(left == -1 && right == -1) {
#ifdef TRACK_TRAVERSALS
				target[threadIdx.x].num_trunc++;
#endif
				continue;
			}

			unsigned int vote_left = __ballot(curr_mask && (dist < node.threshold));
			unsigned int vote_right = __ballot(curr_mask && (dist >= node.threshold));
			if(__popc(vote_left) > __popc(vote_right)) {
				sp ++;
                node_stack[WARP_INDEX][sp] = right;
				stk_mask[sp] = curr_mask;
                sp ++;
				node_stack[WARP_INDEX][sp] = left;
				stk_mask[sp] = curr_mask;
			} else {
                sp ++;
				node_stack[WARP_INDEX][sp] = left;
				stk_mask[sp] = curr_mask;
                sp ++;
				node_stack[WARP_INDEX][sp] = right;
				stk_mask[sp] = curr_mask;
			}
		}

		__GPU_point_Point_d[__GPU_point_array_d[pidx].target] = target[threadIdx.x];
	}
}
