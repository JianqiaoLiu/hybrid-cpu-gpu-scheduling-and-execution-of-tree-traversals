/* -*- mode: C++ -*- */

#include "vptree.h"

__global__ void init_kernel(void) {
	return;
}


__global__ void search_kernel(struct __GPU_tree d_tree, struct Point *__GPU_point_Point_d, struct __GPU_point *__GPU_point_array_d, struct Point *__GPU_node_point_d) {
	
	int pidx;
	//struct Point *target;

	int sp;
	int cur_node_index;

	__shared__ int node_stack[NWARPS_PER_BLOCK][128];

	struct __GPU_Node node;
	struct __GPU_Node parent_node; // structs cached into registers

	__shared__ struct Point target[THREADS_PER_BLOCK]; // point data cached in SMEM

	for (pidx = blockIdx.x * blockDim.x + threadIdx.x; pidx < d_tree.npoints;
       pidx += blockDim.x * gridDim.x) {
		
		target[threadIdx.x] = __GPU_point_Point_d[__GPU_point_array_d[pidx].target];
		sp = 0;
		node_stack[WARP_INDEX][0] = 0;
		
		while(sp >= 0) {
			
			cur_node_index = node_stack[WARP_INDEX][sp--];
			if(cur_node_index == -1) {
				continue;
			}

			node = d_tree.nodes[cur_node_index];

#ifdef TRACK_TRAVERSALS
			target[threadIdx.x].num_nodes_traversed++;
#endif


			int parent_node_index = node.parent;
			
			if(parent_node_index != -1) {
				parent_node = d_tree.nodes[parent_node_index];
				float upperDist = 0.0;
				int i;
				struct Point *a = &__GPU_node_point_d[parent_node.point];				
				for(i = 0; i < DIM; i++) {
					float diff = (a->coord[i] - target[threadIdx.x].coord[i]);
					upperDist += (diff*diff);
				}
				upperDist = sqrt(upperDist);
				
				if(parent_node.right == cur_node_index) {
					if(__all(upperDist + target[threadIdx.x].tau < parent_node.threshold)) {
#ifdef TRACK_TRAVERSALS
						target[threadIdx.x].num_trunc++;
#endif
						continue;
					}
				} else if(parent_node.left == cur_node_index) {
					if(__all(upperDist - target[threadIdx.x].tau > parent_node.threshold)) {
#ifdef TRACK_TRAVERSALS
						target[threadIdx.x].num_trunc++;
#endif
						continue;
					}
				}				
			}

			float dist = 0.0;
			int i;
			struct Point *a = &__GPU_node_point_d[node.point];				
			for(i = 0; i < DIM; i++) {
				float diff = (a->coord[i] - target[threadIdx.x].coord[i]);
				dist += diff * diff;
			}
			dist = sqrt(dist);

			if(dist < target[threadIdx.x].tau) {
				target[threadIdx.x].closest_label = __GPU_node_point_d[node.point].label;
				target[threadIdx.x].tau = dist;
			}

			int left = node.left; // cache to registers (CSE)
			int right = node.right;
			if(left == -1 && right == -1) {
#ifdef TRACK_TRAVERSALS
				target[threadIdx.x].num_trunc++;
#endif
				continue;
			}

			unsigned int vote_left = __ballot(dist < node.threshold);
			unsigned int vote_right = ~vote_left;
			if(__popc(vote_left) > __popc(vote_right)) {
				node_stack[WARP_INDEX][++sp] = right;
				node_stack[WARP_INDEX][++sp] = left;
			} else {
				node_stack[WARP_INDEX][++sp] = left;
				node_stack[WARP_INDEX][++sp] = right;
			}
		}

		__GPU_point_Point_d[__GPU_point_array_d[pidx].target] = target[threadIdx.x];
	}
}
