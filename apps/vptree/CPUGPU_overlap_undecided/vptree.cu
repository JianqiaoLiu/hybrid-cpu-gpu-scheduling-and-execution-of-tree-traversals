/* -*- mode: c++ -*- */
// A VP-Tree implementation, by Steve Hanov. (steve.hanov@gmail.com)
// Released to the Public Domain
// Based on "Data Structures and Algorithms for Nearest Neighbor Search" by Peter N. Yianilos
#include "ptrtab.h"
#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <stdarg.h>
#include <float.h>
#include <getopt.h>
#include "../../../common/util_common.h"
#include <vector>
using namespace std;

#include "vptree.h"
#include "ptrtab.h"

struct Point *__GPU_point_Point_d;
struct Point *__GPU_point_Point_h;
struct __GPU_point *__GPU_point_array_d;
struct __GPU_point *__GPU_point_array_h;
struct Point *__GPU_node_point_d;
struct Point *__GPU_node_point_h;
struct __GPU_tree __the_tree_h;
struct __GPU_tree __the_tree_d;

pthread_mutex_t count_mutex     = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  condition_var   = PTHREAD_COND_INITIALIZER;
int nStreams = 0;
cudaStream_t* stream;

struct Point **_items;
int npoints;
int verbose_flag;
int check_flag;
int sort_flag;
int ratio_flag = 0;
int warp_flag = 0;
int nthreads;
struct Node *_root;
vector<int>::iterator cluster_iter;
vector<int>* DATA;

TIME_INIT(search_kernel);
TIME_INIT(kernel);
TIME_INIT(pre_kernel);
TIME_INIT(init_kernel);
TIME_INIT(search);
TIME_INIT(extra);
int* cpu_buffer;

void check_depth(Node * node, int _depth);
void Sort(int _count, vector<int> &_upper, int** _buffer, int _offset, int _level, int _total, int* h_correlation_matrix);
static int DEPTH_STAT = 0;
struct Point** points;
struct Point** searchpoints;

int compare_point(const void *p1, const void *p2) {
  const struct Point *pp1 =  *((const struct Point **)p1);
  const struct Point *pp2 =  *((const struct Point **)p2);
  if ((pp1 -> vantage_dist) < (pp2 -> vantage_dist)) 
    return -1;
  else 
    return 1;
}

struct Node* buildFromPoints(struct Point **_items, int lower, int upper ) {
	int i, j;
	struct Point *ptmp;
	
	#ifdef TRACK_TRAVERSALS
	static int node_id = 0;
	#endif

	if ( upper == lower ) {
		return NULL;
	}

	struct Node* node = NULL;
	SAFE_MALLOC(node, sizeof(struct Node));

	node->point = _items[lower];
	node->left = NULL;
	node->right = NULL;
	node->parent = NULL;
	node->threshold = 0.0;
	#ifdef TRACK_TRAVERSALS
	node->id = node_id++;
	#endif

	if ( upper - lower > 1 ) {

		// choose an arbitrary point and move it to the start
		// This is one of several ways to find the best candidate VP
		i = lower; //(int)((float)rand() / RAND_MAX * (upper - lower - 1) ) + lower;
		ptmp = _items[lower];
		_items[lower] = _items[i];
		_items[i] = ptmp;

		int median = ( upper + lower ) / 2;

		// partitian around the median distance		
		for(i = lower + 1; i < upper; i++) {
			_items[i]->vantage_dist = mydistance(_items[lower], _items[i]);
		}

		qsort(&_items[lower + 1], upper - lower - 1, sizeof(struct Point *), compare_point);

		// what was the median?
		node->threshold = mydistance( _items[lower], _items[median]);

		node->point = _items[lower];
		node->left = buildFromPoints(_items, lower + 1, median );

		if (node->left != NULL) 
			node->left->parent = node;
		
		node->right = buildFromPoints(_items, median, upper );

		if (node->right != NULL) 
			node->right->parent = node;
	}

	return node;
}

// to avoid conflict with std::distance :/
float mydistance(struct Point *a,struct Point *b) {
  float d = 0.0;
  int i;
  for (i = 0; i < DIM; i++) {
    float diff = ((a -> coord)[i] - (b -> coord)[i]);
    d += (diff * diff);
  }
  return (sqrt(d));
}

void *search_entry(void *args)
{
	targs *ta = (targs *)args;
	struct Point *target;
	int i;
	int *h_correlation_matrix = NULL;
	int *d_correlation_matrix = NULL;

	dim3 blocks(NUM_THREAD_BLOCKS);
	dim3 tpb(THREADS_PER_BLOCK);

	// added by Cambridge
	int _thread_npoints = (ta -> ub) - (ta -> lb);
	long nMatrixSize = _thread_npoints * DEPTH_STAT;
	printf("npoints = %d, DEPTH_STAT = %d, nMatrixSize = %d from thread %d.\n", _thread_npoints, DEPTH_STAT, nMatrixSize, ta -> tid);
	SAFE_MALLOC(h_correlation_matrix, sizeof(int) * nMatrixSize);  
    CUDA_SAFE_CALL(cudaMalloc(&(d_correlation_matrix), sizeof(int) * nMatrixSize));
	memset(h_correlation_matrix, -1, sizeof(int) * nMatrixSize);  

//	pthread_mutex_lock( &count_mutex );
	CUDA_SAFE_CALL(cudaMemcpyAsync(d_correlation_matrix, h_correlation_matrix, sizeof(int) * nMatrixSize, cudaMemcpyHostToDevice, stream[ta -> tid]));
	printf("pre kernel start! tid = %d\n", ta -> tid);
	search_pre_kernel<<<blocks, tpb, 0, stream[ta -> tid]>>> (__the_tree_d, __GPU_point_Point_d, __GPU_point_array_d, __GPU_node_point_d, d_correlation_matrix, ta -> tid, nthreads);
															 
/*	cudaError_t err = cudaThreadSynchronize();
	if(err != cudaSuccess) {
		fprintf(stderr,"Kernel failed with error: %s\n", cudaGetErrorString(err));
		exit(1);
	}*/
	CUDA_SAFE_CALL(cudaMemcpyAsync(h_correlation_matrix, d_correlation_matrix, sizeof(int) * nMatrixSize, cudaMemcpyDeviceToHost, stream[ta -> tid]));

	cudaError_t err = cudaStreamSynchronize(stream[ta -> tid]);
	if(err != cudaSuccess) {
		fprintf(stderr,"Kernel failed with error: %s\n", cudaGetErrorString(err));
		exit(1);
	}

//	pthread_mutex_unlock( &count_mutex );
	printf("pre kernel end! tid = %d\n", ta -> tid);

	int bytes = _thread_npoints * sizeof(int);
//	int* cpu_buffer;
	int* gpu_buffer;
	SAFE_MALLOC(cpu_buffer, bytes);
	memset(cpu_buffer, 0, bytes);
    CUDA_SAFE_CALL(cudaMalloc(&(gpu_buffer), bytes));

	vector<int> DATA;
	DATA.reserve(_thread_npoints);
	for (int id = 0; id < _thread_npoints; id ++)
	{
		DATA.push_back(id);
	}
	Sort(_thread_npoints, DATA, &cpu_buffer, 0, 0, _thread_npoints, h_correlation_matrix);

//	pthread_mutex_lock( &count_mutex );
	CUDA_SAFE_CALL(cudaMemcpyAsync(gpu_buffer, cpu_buffer, bytes, cudaMemcpyHostToDevice, stream[ta -> tid + nthreads]));
	printf("kernel start! tid = %d\n", ta -> tid);

	search_kernel<<<blocks, tpb, 0, stream[ta -> tid + nthreads]>>> (__the_tree_d, __GPU_point_Point_d, __GPU_point_array_d, __GPU_node_point_d, gpu_buffer, ta -> tid, nthreads);
//	cudaThreadSynchronize();
	cudaError_t e = cudaStreamSynchronize(stream[ta -> tid + nthreads]);
	if(e != cudaSuccess) {
		fprintf(stderr, "Error: search_kernel failed with error: %s\n", cudaGetErrorString(e));
		exit(1);
	}
	printf("kernel end! tid = %d\n", ta -> tid);
//	pthread_mutex_unlock( &count_mutex );
	pthread_exit(0);
	return 0;
}

struct Point *read_point(FILE *in) {
#ifdef TRACK_TRAVERSALS
	static int id = 0;
#endif

	struct Point *p;
	SAFE_MALLOC(p, sizeof(struct Point));
		
	if(fscanf(in, "%d", &p->label) != 1) {
		fprintf(stderr, "Input file not large enough.\n");
		exit(1);
	}
	int j;
	for(j = 0; j < DIM; j++) {
		if(fscanf(in, "%f", &p->coord[j]) != 1) {
			fprintf(stderr, "Input file not large enough.\n");
			exit(1);
		}
	}

#ifdef TRACK_TRAVERSALS
	p->num_nodes_traversed = 0;
	p->num_trunc = 0;
	p->id = id++;
#endif

	return p;
}

struct Point *gen_point() {
#ifdef TRACK_TRAVERSALS
	static int id = 0;
#endif

	struct Point *p;
	SAFE_MALLOC(p, sizeof(struct Point));
	int j;
	p->label=0;
	for (j = 0; j < DIM; j++) {
		p->coord[j] = (float)rand() / RAND_MAX;
	}

#ifdef TRACK_TRAVERSALS
	p->num_nodes_traversed = 0;
	p->num_trunc = 0;
	p->id = id++;
#endif

	return p;
}

void read_input(int argc, char **argv, struct Point*** p_points, struct Point*** p_searchpoints) {
	
	int i, c;
	struct Point **points;
	struct Point **searchpoints;

	check_flag = 0;
	sort_flag = 0;
	verbose_flag = 0;
	nthreads = 1;
	i=0;
	while((c = getopt(argc, argv, "cvt:srw")) != -1) {
		switch(c) {
		case 'c':
			check_flag = 1;
			i++;
			break;

		case 'v':
			verbose_flag = 1;
			i++;
			break;

		case 't':
			nthreads = atoi(optarg);
			if(nthreads <= 0) {
				fprintf(stderr, "Error: invalid number of threads.\n");
				exit(1);
			}
			i+=2;
			break;

		case 's':
			sort_flag = 1;
			i++;
			break;
        
        case 'r':
            ratio_flag = 1;
            i ++;
            break;

        case 'w':
            warp_flag = 1;
            i ++;
            break;

		case '?':
			fprintf(stderr, "Error: unknown option.\n");
			exit(1);
			break;

		default:
			abort();
		}
	}
 
	if(argc - i < 2 || argc - i > 3) {
		fprintf(stderr, "usage: vptree [-c] [-v] [-t <nthreads>] [-s] <npoints> [input_file]\n");
		exit(1);
	}

	char *input_file = NULL;
	for(i = optind; i < argc; i++) {
		switch(i - optind) {
		case 0:
			input_file = argv[i];
			break;

		case 1:
			npoints = atoi(argv[i]);
			if(npoints <= 0) {
				fprintf(stderr, "Invalid number of points.\n");
				exit(1);
			}
			break;
		}
	}

	printf("Configuration: sort_flag = %d, verbose_flag = %d, nthreads=%d, DIM = %d, npoints = %d, input_file=%s\n", sort_flag, verbose_flag, nthreads, DIM, npoints, input_file);

	// Allocate the point and search point arrays
	SAFE_CALLOC(points, npoints, sizeof(struct Point*));
	SAFE_CALLOC(searchpoints, npoints, sizeof(struct Point*));

	if (input_file != NULL) {
		FILE *in = fopen(input_file, "r");
		if( in == NULL) {
			fprintf(stderr, "Could not open %s\n", input_file);
			exit(1);
		}

		for (i = 0; i < npoints; i++) {
			points[i] = read_point(in);
		}

		for (i = 0; i < npoints; i++) {
			searchpoints[i] = read_point(in);
		}

		fclose(in);
	} else {
		for (i = 0; i < npoints; i++) {
			points[i] = gen_point();			
		}
		for (i = 0; i < npoints; i++) {
			searchpoints[i] = gen_point();
		}
	}
	
	*p_points = points;
	*p_searchpoints = searchpoints;
}

int main( int argc, char* argv[] ) {
	srand(0);
	int i;

	read_input(argc, argv, &points, &searchpoints);
	
	_items = points;
	_root = buildFromPoints(points, 0, npoints);	

	TIME_START(extra);
	check_depth(_root, 0);
	TIME_END(extra);

	if(sort_flag) {
		buildFromPoints(searchpoints, 0, npoints);
	}

	TIME_START(init_kernel);
	init_kernel<<<1,1>>>();
	TIME_END(init_kernel);
	TIME_ELAPSED_PRINT(init_kernel, stdout);

	//print_tree(_root);

	int correct_cnt = 0;
	int nsearchpoints = npoints; 	

	int rc;
	pthread_t * threads;
	SAFE_MALLOC(threads, sizeof(pthread_t)*nthreads);
	
	targs * args;
	SAFE_MALLOC(args, sizeof(targs)*nthreads);

	// Assign points to threads
	int start = 0;
	int j;
	for(j = 0; j < nthreads; j++) {
		int num = (npoints - start) / (nthreads - j);
		args[j].searchpoints = searchpoints;
		args[j].tid = j;
		args[j].lb = start;
		args[j].ub = start + num;
		start += num;
		//printf("%d %d\n", args[j].lb, args[j].ub);
	}

	TIME_START(search);

	__GPU_point_array_h = ((struct __GPU_point *)(malloc(sizeof(struct __GPU_point ) * (npoints))));
	if (__GPU_point_array_h == 0) {
		fprintf(stderr,"error [file=%s line=%d]: %s is NULL!","transformation",0,"__GPU_point_array_h");
		abort();
	}
	__GPU_point_Point_h = ((struct Point *)(malloc(sizeof(struct Point ) * (npoints))));
	if (__GPU_point_Point_h == 0) {
		fprintf(stderr,"error [file=%s line=%d]: %s is NULL!","transformation",0,"__GPU_point_Point_h");
		abort();
	}
	__GPU_node_point_h = ((struct Point *)(malloc(sizeof(struct Point ) * (npoints))));
	if (__GPU_node_point_h == 0) {
		fprintf(stderr,"error [file=%s line=%d]: %s is NULL!","transformation",0,"__GPU_node_point_h");
		abort();
	}

	struct Point *target;
	for (int i = 0; i < npoints; i++) {
		target = searchpoints[i];
		target -> tau = 3.40282347e+38F;
		memcpy(__GPU_point_Point_h + i,target,sizeof(struct Point ) * 1);
		__GPU_point_array_h[i].target = i;
	}

	__the_tree_h = __GPU_buildTree(_root,npoints);

	__the_tree_d = __GPU_allocDeviceTree(__the_tree_h);
	__GPU_memcpyTreeToDev(__the_tree_h,__the_tree_d);

	TIME_START(search_kernel);
	nStreams = nthreads * 2;
	stream = (cudaStream_t*)malloc(sizeof(cudaStream_t) * nStreams);
	for (int i = 0; i < nStreams; ++i) {
		CUDA_SAFE_CALL( cudaStreamCreate(&stream[i]) );
	}
	for(i = 0; i < nthreads; i++) {		
		rc = pthread_create(&threads[i], NULL, search_entry, &args[i]);
		if(rc) {
			fprintf(stderr, "Error: could not create thread, rc = %d\n", rc);
			exit(1);
		}
	}
	
	// wait for threads
	for(i = 0; i < nthreads; i++) {
		pthread_join(threads[i], NULL);
	}

	TIME_END(search_kernel);
	TIME_ELAPSED_PRINT(search_kernel, stdout);

	__GPU_memcpyTreeToHost(__the_tree_h, __the_tree_d);

	for (i = 0; i < npoints; i++) {
		target = searchpoints[i];
		memcpy(target, __GPU_point_Point_h + i,sizeof(struct Point ) * 1);
	}

	__GPU_freeDeviceTree(__the_tree_d);

	// compute correct count
	for (i = 0; i < npoints; i++) {
		struct Point *target = searchpoints[i];
		if (target->label == target->closest_label) {
			correct_cnt++;
		}
	}
	
	TIME_END(search);
	TIME_ELAPSED_PRINT(search, stdout);

#ifdef TRACK_TRAVERSALS
	unsigned long long sum_nodes_traversed = 0;
	int sum_trunc = 0;
	int na;
	int maximum = 0, all = 0;
	unsigned long long maximum_sum = 0, all_sum = 0;
	for(i = 0; i < nsearchpoints + (nsearchpoints % 32); i+=32) {
//		struct Point* p = searchpoints[i];
		struct Point* p = searchpoints[cpu_buffer[i]];
		sum_nodes_traversed += p->num_nodes_traversed;
		sum_trunc += p->num_trunc;
		na = p->num_nodes_traversed;
   
        for(j = i + 1; j < i + 32 && j < nsearchpoints; j++) {
//          p = searchpoints[j];
            p = searchpoints[cpu_buffer[j]];
            if(p->num_nodes_traversed > na)
               na = p->num_nodes_traversed;
            sum_nodes_traversed += p->num_nodes_traversed;
            sum_trunc += p->num_trunc;

        }

        if (warp_flag) {
            p = searchpoints[cpu_buffer[i]];
		    maximum = p->num_nodes_traversed;
	    	all = p->num_nodes_traversed;
    		for(j = i + 1; j < i + 32 && j < nsearchpoints; j++) {
//			    p = searchpoints[j];
			    p = searchpoints[cpu_buffer[j]];

			    if (p->num_nodes_traversed > maximum)
				    maximum = p->num_nodes_traversed;
			    all += p->num_nodes_traversed;
		    }
//		    printf("nodes warp %d: %d\n", i/32, na);
		    printf("%d\n", maximum);
		    maximum_sum += maximum;
		    all_sum += all;
        }
	}
	printf("@ maximum_sum: %llu\n", maximum_sum);
	printf("@ all_sum: %llu\n", all_sum);

	printf("@ sum_nodes_traversed: %llu\n", sum_nodes_traversed);
	printf("@ avg_nodes_traversed: %f\n", (float)sum_nodes_traversed / nsearchpoints);
	printf("sum_trunc:%d\n", sum_trunc);
#endif

	float correct_rate = (float) correct_cnt / nsearchpoints;
	printf("correct rate: %.4f\n", correct_rate);

	// TODO: free the rest but its not really important
	/*
	for(i = 0; i < npoints; i++) {
		free(points[i]);
		free(searchpoints[i]);
	}
	free(points);
	free(searchpoints);
	*/
	return 0;
}

void check_depth(Node * node, int _depth)
{
	if (!node)
	{
		return;
	}

	node->depth = _depth;
	if (node->depth == SPLICE_DEPTH)
	{
		node->pre_id = DEPTH_STAT ++;
		return;
	}

	check_depth(node->left, _depth + 1);
	check_depth(node->right, _depth + 1);
}

void Sort(int _count, vector<int> &_upper, int** _buffer, int _offset, int _level, int _total, int* h_correlation_matrix )
{
	vector<int>* clusters;
	clusters = new vector<int> [DEPTH_STAT];
	int temp = 0;
	int pos = 0;
	int buffer_index = _offset;
	for(int point = 0; point < _count; point ++)
	{
		pos = _upper[point] + _level * _total;
		temp = h_correlation_matrix[pos];
		if (temp != -1)
		{
			clusters[temp].push_back(_upper[point]);
		}
		else
		{
			(*_buffer)[buffer_index ++] = _upper[point];
		}
	}


	int bytes = _count * sizeof(int);
	for(int group = 0; group < DEPTH_STAT; group ++)
	{
		if ( clusters[group].size() != 0)
		{
			//if (clusters[group].size() < 1000 || _level >= SPLICE_DEPTH)
			if (_level >= SPLICE_DEPTH || clusters[group].size() <= 32)
            {
				for(cluster_iter = clusters[group].begin(); cluster_iter != clusters[group].end(); cluster_iter ++)
				{
					(*_buffer)[buffer_index ++] = *cluster_iter;
				}
//			printf("level = %d, node = %d, size = %d, buffer_index = %d.\n", _level, group, clusters[group].size(), buffer_index);
			}
			else
			{
                Sort(clusters[group].size(), clusters[group], _buffer, buffer_index, _level + 1, _total, h_correlation_matrix);
                buffer_index += clusters[group].size();
			}
		}
	}
	
	for(int i = 0; i < DEPTH_STAT; i ++)
	{
		clusters[i].clear();
	}
	delete [] clusters;
}
