/* -*- mode: C++ -*- */

#include "vptree.h"

__global__ void init_kernel(void) {
	return;
}


__global__ void search_kernel(struct __GPU_tree d_tree, struct Point *__GPU_point_Point_d, struct __GPU_point *__GPU_point_array_d, struct Point *__GPU_node_point_d, int* index_buffer) {

	int pidx, idx;
	//struct Point *target;

	int sp;
	int cur_node_index;

	int node_stack[128];

	struct __GPU_Node node;
	struct __GPU_Node parent_node; // structs cached into registers
	__shared__ struct Point target[THREADS_PER_BLOCK]; // point data cached in SMEM

//	for (pidx = blockIdx.x * blockDim.x + threadIdx.x; pidx < d_tree.npoints; pidx += blockDim.x * gridDim.x) {
    for (idx = blockIdx.x * blockDim.x + threadIdx.x; idx < d_tree.npoints; idx += blockDim.x * gridDim.x) {
        pidx = index_buffer[idx];
		
		target[threadIdx.x] = __GPU_point_Point_d[__GPU_point_array_d[pidx].target];
		sp = 0;
		node_stack[0] = 0;
		
		int d_matrix_index = pidx;
		while(sp >= 0) {
			cur_node_index = node_stack[sp--];
			if(cur_node_index == -1) {
				continue;
			}

			node = d_tree.nodes[cur_node_index];

#ifdef TRACK_TRAVERSALS
			target[threadIdx.x].num_nodes_traversed++;
#endif
			int parent_node_index = node.parent;
//            printf("pidx = %d, parent_node_index = %d.\n", pidx, parent_node_index);

            parent_node = d_tree.nodes[parent_node_index];
			if(parent_node_index != -1) {
				float upperDist = 0.0;
				int i;
				struct Point *a = &__GPU_node_point_d[parent_node.point];				
				for(i = 0; i < DIM; i++) {
					float diff = (a->coord[i] - target[threadIdx.x].coord[i]);
					upperDist += (diff*diff);
				}
				upperDist = sqrt(upperDist);
				
				if(parent_node.right == cur_node_index) {
					if(upperDist + target[threadIdx.x].tau < parent_node.threshold) {
#ifdef TRACK_TRAVERSALS
						target[threadIdx.x].num_trunc++;
#endif
						continue;
					}
				} else if(parent_node.left == cur_node_index) {
					if(upperDist - target[threadIdx.x].tau > parent_node.threshold) {
#ifdef TRACK_TRAVERSALS
						target[threadIdx.x].num_trunc++;
#endif
						continue;
					}
				}				
			}

			float dist = 0.0;
			int i;
			struct Point *a = &__GPU_node_point_d[node.point];				
			for(i = 0; i < DIM; i++) {
				float diff = (a->coord[i] - target[threadIdx.x].coord[i]);
				dist += diff * diff;
			}
			dist = sqrt(dist);

			if(dist < target[threadIdx.x].tau) {
				target[threadIdx.x].closest_label = __GPU_node_point_d[node.point].label;
				target[threadIdx.x].tau = dist;
			}

			int left = node.left; // cache to registers (CSE)
			int right = node.right;
			if(left == -1 && right == -1) {
#ifdef TRACK_TRAVERSALS
				target[threadIdx.x].num_trunc++;
#endif
				continue;
			}

			if(dist < node.threshold) {
				node_stack[++sp] = right;
				node_stack[++sp] = left;
			} else {
				node_stack[++sp] = left;
				node_stack[++sp] = right;
			}
		}

		__GPU_point_Point_d[__GPU_point_array_d[pidx].target] = target[threadIdx.x];
	}
}


__global__ void search_pre_kernel(struct __GPU_tree d_tree, struct Point *__GPU_point_Point_d, struct __GPU_point *__GPU_point_array_d, struct Point *__GPU_node_point_d, int* d_matrix) {
	
	int pidx;
	//struct Point *target;

	int sp;
	int cur_node_index;

	int node_stack[128];

	struct __GPU_Node node;
	struct __GPU_Node parent_node; // structs cached into registers
	__shared__ struct Point target[THREADS_PER_BLOCK]; // point data cached in SMEM

	for (pidx = blockIdx.x * blockDim.x + threadIdx.x; pidx < d_tree.npoints;
       pidx += blockDim.x * gridDim.x) {
		
		target[threadIdx.x] = __GPU_point_Point_d[__GPU_point_array_d[pidx].target];
		sp = 0;
		node_stack[0] = 0;
		
		int d_matrix_index = pidx;
		while(sp >= 0) {
			cur_node_index = node_stack[sp--];
			if(cur_node_index == -1) {
				continue;
			}

			node = d_tree.nodes[cur_node_index];
			if (node.depth == SPLICE_DEPTH)
			{
				d_matrix[d_matrix_index] = node.pre_id;
				d_matrix_index += d_tree.npoints;
//				d_matrix[node.pre_id * d_tree.npoints + pidx] = 1;
			}

#ifdef TRACK_TRAVERSALS
			target[threadIdx.x].num_nodes_traversed++;
#endif
			int parent_node_index = node.parent;
//            printf("pidx = %d, parent_node_index = %d.\n", pidx, parent_node_index);

            parent_node = d_tree.nodes[parent_node_index];
			if(parent_node_index != -1) {
				float upperDist = 0.0;
				int i;
				struct Point *a = &__GPU_node_point_d[parent_node.point];				
				for(i = 0; i < DIM; i++) {
					float diff = (a->coord[i] - target[threadIdx.x].coord[i]);
					upperDist += (diff*diff);
				}
				upperDist = sqrt(upperDist);
				
				if(parent_node.right == cur_node_index) {
					if(upperDist + target[threadIdx.x].tau < parent_node.threshold) {
#ifdef TRACK_TRAVERSALS
						target[threadIdx.x].num_trunc++;
#endif
						continue;
					}
				} else if(parent_node.left == cur_node_index) {
					if(upperDist - target[threadIdx.x].tau > parent_node.threshold) {
#ifdef TRACK_TRAVERSALS
						target[threadIdx.x].num_trunc++;
#endif
						continue;
					}
				}				
			}

			float dist = 0.0;
			int i;
			struct Point *a = &__GPU_node_point_d[node.point];				
			for(i = 0; i < DIM; i++) {
				float diff = (a->coord[i] - target[threadIdx.x].coord[i]);
				dist += diff * diff;
			}
			dist = sqrt(dist);

			if(dist < target[threadIdx.x].tau) {
				target[threadIdx.x].closest_label = __GPU_node_point_d[node.point].label;
				target[threadIdx.x].tau = dist;
			}

			int left = node.left; // cache to registers (CSE)
			int right = node.right;
			if(left == -1 && right == -1) {
#ifdef TRACK_TRAVERSALS
				target[threadIdx.x].num_trunc++;
#endif
				continue;
			}

			if (node.depth < SPLICE_DEPTH)
			{
				if(dist < node.threshold) {
					node_stack[++sp] = right;
					node_stack[++sp] = left;
				} else {
					node_stack[++sp] = left;
					node_stack[++sp] = right;
				}
			}
		}

//		__GPU_point_Point_d[__GPU_point_array_d[pidx].target] = target[threadIdx.x];
	}
}
