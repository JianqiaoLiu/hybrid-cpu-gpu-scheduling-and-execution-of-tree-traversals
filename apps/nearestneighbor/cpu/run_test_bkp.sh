#! /bin/sh -x

INPUT_SIZE=200000
INPUT_DIR=/home/min/a/liu1274/cuda-workspace/FirstPaper/point_corr/input
INPUT_DIM=7
INPUT_FILES=($INPUT_DIR/covtype.7d)
NUM_RUNS=1
THREADS=32
MAKE_ARGS=(TRACK_TRAVERSALS=1)

make clean;
make ${MAKE_ARGS[*]} DIM=${INPUT_DIM[$j]}

for ((i = 0; i < NUM_RUNS; i++))
do
	./nn -s -t $THREADS ${INPUT_FILES} ${INPUT_SIZE} ${INPUT_SIZE} 2>&1
	./nn -t $THREADS ${INPUT_FILES} ${INPUT_SIZE} ${INPUT_SIZE} 2>&1
done

