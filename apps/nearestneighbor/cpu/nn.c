#include "nn.h"

int sort_flag = 0;
int verbose_flag = 0;
int check_flag = 0;
int nthreads = 1;

Point *training_points;
KDCell *root;
Point *search_points;

int npoints;
int nsearchpoints;
char *input_file;

static inline float distance_axis(Point *a, Point *b, int axis);
static inline float distance(Point *a, Point *b);

static void* thread_function(void *arg);

TIME_INIT(runtime);
TIME_INIT(construct_tree);
TIME_INIT(traversal_time);
int main(int argc, char **argv) {

	int correct_cnt, i, j;
	long long sum_nodes_traversed;
	float correct_rate;
	
	struct thread_args *args;
	pthread_t *threads;

	read_input(argc, argv);
	printf("configuration: sort_flag=%d verbose_flag=%d check_flag=%d DIM = %d nthreads = %d npoints = %d nsearchpoints = %d\n", sort_flag, verbose_flag, check_flag, DIM, nthreads, npoints, nsearchpoints);

	TIME_START(runtime);
	TIME_START(construct_tree);

	if(sort_flag) {
		sort_points(search_points, 0, nsearchpoints - 1, 0);
	}
	root = construct_tree(training_points, 0, npoints - 1, 0, 1);
	
	TIME_END(construct_tree);
	TIME_START(traversal_time);

	SAFE_MALLOC(args, sizeof(struct thread_args)*nthreads);
	SAFE_MALLOC(threads, sizeof(pthread_t)*nthreads);
	
	// Assign points to threads
	int start = 0;
	for(j = 0; j < nthreads; j++) {
		int num = (nsearchpoints - start) / (nthreads - j);
		args[j].tid = j;
		args[j].lb = start;
		args[j].ub = start + num;
		start += num;

		//printf("%d %d\n", args[j].lb, args[j].ub);
	}
	
	for(j = 0; j < nthreads; j++) {
		if(pthread_create(&threads[j], NULL, thread_function, &args[j]) != 0) {
			fprintf(stderr, "Could not create thread %d\n", j);
			exit(1);
		}
	}

	for(j = 0; j < nthreads; j++) {
		pthread_join(threads[j], NULL);
	}
	
	free(args);
	free(threads);
 	
	TIME_END(traversal_time);
	TIME_END(runtime);

	correct_cnt = 0;
	for(i = 0; i < nsearchpoints; i++) {
			if(search_points[i].closest >= 0) {
				if (training_points[search_points[i].closest].label == search_points[i].label) {
					correct_cnt++;
				}
		}
	}
	correct_rate = (float) correct_cnt / nsearchpoints;
	printf("correct rate: %.4f\n", correct_rate);

	sum_nodes_traversed = 0;
	for (i = 0; i < nsearchpoints; i++) {
		sum_nodes_traversed += search_points[i].numNodesTraversed;
//		printf("%d %d\n", i, search_points[i].numNodesTraversed);
	}
	printf("sum_nodes_traversed:%ld\n", sum_nodes_traversed);

	// print results
	if(verbose_flag) {
		for(i = 0; i < nsearchpoints; i++) {
			printf("%d: %d (%2.3f)\n", i, training_points[search_points[i].closest].label, search_points[i].closestDist);
		}
	}
	
	TIME_ELAPSED_PRINT(construct_tree, stdout);
	TIME_ELAPSED_PRINT(traversal_time, stdout);
	TIME_ELAPSED_PRINT(runtime, stdout);

	return 0;
}

void* thread_function(void *arg) {
	
	struct thread_args *args = (struct thread_args*)arg;
	int j = 0;
	for(j = args->lb; j < args->ub; j++) {
		nearest_neighbor_search(&search_points[j], root);	 		
	}
}

void update_closest(Point *point, int candidate_index) {
	float dist = distance(point, &training_points[candidate_index]);
	Point *tmppt;
	float tmpdist;
	int n;

	if (dist > point->closestDist) {
		return;
	}

	point->closest = candidate_index;
	point->closestDist = dist;
//		printf("%d.\n", candidate_index);
}

void nearest_neighbor_search(Point *point, KDCell *node) {
	int i;
	Point *candidate;
	point->numNodesTraversed++;
	if(node == NULL)
		return;
//    printf("I'm visiting %dth node: %f, %f ,%f, %f, %f, %f, %f.\n", node->id, node->max[0], node->max[1], node->max[2], node->max[3], node->min[4], node->min[5], node->min[6]);    	
// is this node closer than the current best?
	if(!can_correlate(point, node, point->closestDist)) {
        return;
	}

	if(node->axis == DIM) {
		for (i = 0; i < MAX_POINTS_IN_CELL; i++) {
			if (node->points[i] >= 0) {
				update_closest(point, node->points[i]);
			}
		}
	} else {
		if(point->coord[node->axis] < (node->splitval)) {
			nearest_neighbor_search(point, node->left);
			nearest_neighbor_search(point, node->right);

		} else {
			nearest_neighbor_search(point, node->right);
			nearest_neighbor_search(point, node->left);
		}		
	}
}

static inline int can_correlate(Point * point, KDCell * cell, float rad) {
	float dist=0.0;
	float boxdist=0.0;
	float sum=0.0;
	float boxsum=0.0;
	float center=0.0;
	int i;

	for(i = 0; i < DIM; i++) {
		center = (cell->max[i] + cell->min[i]) / 2;
		boxdist = (cell->max[i] - cell->min[i]) / 2;
		dist = point->coord[i] - center;
		sum += dist * dist;
		boxsum += boxdist * boxdist;
	}

	if(sqrt(sum) - sqrt(boxsum) < sqrt(rad))
		return 1;
	else
		return 0;
}


static inline float distance_axis(Point *a, Point *b, int axis) {
	return (a->coord[axis] - b->coord[axis]) * (a->coord[axis] - b->coord[axis]);
}

static inline float distance(Point *a, Point *b) {
	int i;
	float d = 0;
	for(i = 0; i < DIM; i++) {
		d += distance_axis(a,b,i);		
	}
	return d;
}

void read_input(int argc, char **argv) {
	unsigned long long i, j, k, c;
	float min = FLT_MAX;
	float max = FLT_MIN;
	FILE *in;

	if(argc < 4) {
		fprintf(stderr, "usage: nn [-c] [-v] [-t <nthreads>] [-s] <input_file> <npoints> [<nsearchpoints>]\n");
		exit(1);
	}

	while((c = getopt(argc, argv, "cvt:s")) != -1) {
		switch(c) {
		case 'c':
			check_flag = 1;
			break;

		case 'v':
			verbose_flag = 1;
			break;

		case 't':
			nthreads = atoi(optarg);
			if(nthreads <= 0) {
				fprintf(stderr, "Error: invalid number of threads.\n");
				exit(1);
			}
			break;

		case 's':
			sort_flag = 1;
			break;

		case '?':
			fprintf(stderr, "Error: unknown option.\n");
			exit(1);
			break;

		default:
			abort();
		}
	}
	
	for(i = optind; i < argc; i++) {
		switch(i - optind) {
		case 0:
			input_file = argv[i];
			break;

		case 1:
				npoints = atoi(argv[i]);
				nsearchpoints = npoints;
				if(npoints <= 0) {
					fprintf(stderr, "Not enough points.\n");
					exit(1);
				}
				break;

		case 2:
			nsearchpoints = atoi(argv[i]);
			if(nsearchpoints <= 0) {
				fprintf(stderr, "Not enough search points.");
				exit(1);
			}
			break;
		}
	}

	training_points = alloc_points(npoints);
	search_points = alloc_points(nsearchpoints);

	if(strcmp(input_file, "random") == 0) {
		for(i = 0; i < npoints; i++) {
			training_points[i].label = i;
			for(j = 0; j < DIM; j++) {
				training_points[i].coord[j] = 1.0 + (float)rand() / RAND_MAX;			
			}
		}

		for(i = 0; i < nsearchpoints; i++) {
			search_points[i].label = npoints + i;
			for(j = 0; j < DIM; j++) {
				search_points[i].coord[j] = 1.0 + (float)rand() / RAND_MAX;			
			}
		}

	} else {
		in = fopen(input_file, "r");
		if(in == NULL) {
			fprintf(stderr, "Could not open %s\n", input_file);
			exit(1);
		}

		for(i = 0; i < npoints; i++) {
			read_point(in, &training_points[i]);
		}

		for(i = 0; i < nsearchpoints; i++) {
			read_point(in, &search_points[i]);
		}

		fclose(in);
	}
}

Point* alloc_points(int n) {
	int i, j;
	Point *points = malloc(sizeof(Point) * n);
	for (i = 0; i < n; i++) {
		points[i].closestDist = FLT_MAX;
		points[i].closest = -1;
		points[i].numNodesTraversed = 0;
	}
	return points;
}

KDCell* alloc_kdcell() {
	int i;
	KDCell *cell = malloc(sizeof(KDCell));

	for (i = 0; i < DIM; i++) {
		cell->min[i] = FLT_MAX;
		cell->max[i] = FLT_MIN;
	}

	for (i = 0; i < MAX_POINTS_IN_CELL; i++) {
		cell->points[i] = -1;
	}

	cell->left = NULL;
	cell->right = NULL;
	return cell;
}

void read_point(FILE *in, Point *p) {
	int j;
	if(fscanf(in, "%d", &p->label) != 1) {
		fprintf(stderr, "Input file not large enough.\n");
		exit(1);
	}
	for(j = 0; j < DIM; j++) {
		if(fscanf(in, "%f", &p->coord[j]) != 1) {
			fprintf(stderr, "Input file not large enough.\n");
			exit(1);
		}
	}
}
