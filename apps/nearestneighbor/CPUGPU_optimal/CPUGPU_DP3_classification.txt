rm -f *~ *.o *.csv knn_bb
nvcc -arch sm_20 --ptxas-options=-v -I../../ -I. -DTRACK_TRAVERSALS=1 -DDIM=7 -DSPLICE_DEPTH=3 -DTHREADS_NUM=1 -O2 -c knn_bb.cu
nvcc -arch sm_20 --ptxas-options=-v -I../../ -I. -DTRACK_TRAVERSALS=1 -DDIM=7 -DSPLICE_DEPTH=3 -DTHREADS_NUM=1 -O2 -c knn_bb_splice.cpp
nvcc -arch sm_20 --ptxas-options=-v -I../../ -I. -DTRACK_TRAVERSALS=1 -DDIM=7 -DSPLICE_DEPTH=3 -DTHREADS_NUM=1 -O2 -c knn_bb_functions.cu
nvcc -arch sm_20 --ptxas-options=-v -I../../ -I. -DTRACK_TRAVERSALS=1 -DDIM=7 -DSPLICE_DEPTH=3 -DTHREADS_NUM=1 -O2 -c knn_bb_mem.cu
nvcc -arch sm_20 --ptxas-options=-v -I../../ -I. -DTRACK_TRAVERSALS=1 -DDIM=7 -DSPLICE_DEPTH=3 -DTHREADS_NUM=1 -O2 -c knn_bb_pre_kernel.cu
nvcc -arch sm_20 --ptxas-options=-v -I../../ -I. -DTRACK_TRAVERSALS=1 -DDIM=7 -DSPLICE_DEPTH=3 -DTHREADS_NUM=1 -O2 -c knn_bb_kernel.cu
nvcc -o knn_bb knn_bb.o \
							knn_bb_splice.o \
							knn_bb_functions.o \
							knn_bb_mem.o \
							knn_bb_pre_kernel.o \
							knn_bb_kernel.o
configuration: K = 1 DIM = 7 npoints = 200000 nsearchpoints = 200000
the maxnumber of nodes is 16
ROWS = 200000, COLS = 16, nMatrixSize = 3200000.
@ pre_calc:  7 ms
@ pre: 27 ms
we have a new set here:
16th frame with 11991 points:
17th frame with 11421 points:
18th frame with 11809 points:
19th frame with 11444 points:
20th frame with 13521 points:
21th frame with 13076 points:
22th frame with 13173 points:
23th frame with 13486 points:
24th frame with 12894 points:
25th frame with 12192 points:
26th frame with 12620 points:
27th frame with 12329 points:
28th frame with 12452 points:
29th frame with 12843 points:
30th frame with 12231 points:
31th frame with 12518 points:
@ GPU: 2483 ms
@ GPU_sum: 2483 ms
This is the 1th time GPU traversal, and the index is 200000.
we have a new set here:
16th frame with 9824 points:
17th frame with 13429 points:
18th frame with 14442 points:
19th frame with 8970 points:
20th frame with 10682 points:
21th frame with 15977 points:
22th frame with 15995 points:
23th frame with 10602 points:
24th frame with 10456 points:
25th frame with 14493 points:
26th frame with 15203 points:
27th frame with 9883 points:
28th frame with 13231 points:
29th frame with 11518 points:
30th frame with 11316 points:
31th frame with 13979 points:
@ GPU: 1700 ms
@ GPU_sum: 4183 ms
This is the 2th time GPU traversal, and the index is 200000.
we have a new set here:
16th frame with 16770 points:
17th frame with 8111 points:
18th frame with 17788 points:
19th frame with 10587 points:
20th frame with 8256 points:
21th frame with 17243 points:
22th frame with 7888 points:
23th frame with 13278 points:
24th frame with 11203 points:
25th frame with 10165 points:
26th frame with 17293 points:
27th frame with 11383 points:
28th frame with 13941 points:
29th frame with 15327 points:
30th frame with 7528 points:
31th frame with 13239 points:
@ GPU: 1672 ms
@ GPU_sum: 5855 ms
This is the 3th time GPU traversal, and the index is 200000.
we have a new set here:
16th frame with 15632 points:
17th frame with 12743 points:
18th frame with 18509 points:
19th frame with 6372 points:
20th frame with 6176 points:
21th frame with 14990 points:
22th frame with 10804 points:
23th frame with 14695 points:
24th frame with 15279 points:
25th frame with 13397 points:
26th frame with 12727 points:
27th frame with 8641 points:
28th frame with 8426 points:
29th frame with 12341 points:
30th frame with 12271 points:
31th frame with 16997 points:
@ GPU: 1298 ms
@ GPU_sum: 7153 ms
This is the 4th time GPU traversal, and the index is 200000.
we have a new set here:
16th frame with 2448 points:
17th frame with 2551 points:
18th frame with 3717 points:
19th frame with 5426 points:
20th frame with 11862 points:
21th frame with 22719 points:
22th frame with 19523 points:
23th frame with 31833 points:
24th frame with 27799 points:
25th frame with 18553 points:
26th frame with 21168 points:
27th frame with 12760 points:
28th frame with 7293 points:
29th frame with 4150 points:
30th frame with 4426 points:
31th frame with 3772 points:
@ GPU: 1167 ms
@ GPU_sum: 8321 ms
This is the 5th time GPU traversal, and the index is 200000.
we have a new set here:
16th frame with 2988 points:
17th frame with 6155 points:
18th frame with 2951 points:
19th frame with 2048 points:
20th frame with 15442 points:
21th frame with 35914 points:
22th frame with 15115 points:
23th frame with 19466 points:
24th frame with 18588 points:
25th frame with 15340 points:
26th frame with 31848 points:
27th frame with 14504 points:
28th frame with 4761 points:
29th frame with 3437 points:
30th frame with 6812 points:
31th frame with 4631 points:
@ GPU: 811 ms
@ GPU_sum: 9132 ms
This is the 6th time GPU traversal, and the index is 200000.
we have a new set here:
16th frame with 16226 points:
17th frame with 14332 points:
18th frame with 28085 points:
19th frame with 27294 points:
20th frame with 1704 points:
21th frame with 3993 points:
22th frame with 2374 points:
23th frame with 6071 points:
24th frame with 6883 points:
25th frame with 3418 points:
26th frame with 6014 points:
27th frame with 3326 points:
28th frame with 29646 points:
29th frame with 22074 points:
30th frame with 14013 points:
31th frame with 14547 points:
@ GPU: 732 ms
@ GPU_sum: 9864 ms
This is the 7th time GPU traversal, and the index is 200000.
we have a new set here:
16th frame with 23713 points:
17th frame with 31666 points:
18th frame with 19025 points:
19th frame with 11533 points:
20th frame with 1827 points:
21th frame with 6618 points:
22th frame with 2249 points:
23th frame with 3448 points:
24th frame with 5435 points:
25th frame with 3905 points:
26th frame with 7448 points:
27th frame with 2853 points:
28th frame with 15289 points:
29th frame with 13271 points:
30th frame with 26842 points:
31th frame with 24878 points:
@ GPU: 465 ms
@ GPU_sum: 10329 ms
This is the 8th time GPU traversal, and the index is 200000.
@ CPU: 320 ms
correct rate: 0.8632
sum_nodes_traversed:23535897140
@ traversal: 15238 ms
@ overall: 17961 ms
