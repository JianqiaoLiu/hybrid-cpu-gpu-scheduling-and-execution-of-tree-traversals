#ifndef __KNN_BB_PRE_KERNEL_H_
#define __KNN_BB_PRE_KERNEL_H_

#include "knn_bb_data_types.h"
#include "knn_bb_kernel.h"

gpu_tree * pre_transform_tree(KDCell *root);
__global__ void pre_nearest_neighbor_search (kernel_params params, int *d_matrix);

#endif
