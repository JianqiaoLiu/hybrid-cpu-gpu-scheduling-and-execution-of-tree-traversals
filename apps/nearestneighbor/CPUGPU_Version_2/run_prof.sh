#! /bin/bash -x
dim=7
npoints=200000
nsearchpoints=200000
splice_depth=8
threads=1;

make clean;
make TRACK_TRAVERSALS=1 DIM=${dim} SPLICE_DEPTH=${splice_depth} THREADS_NUM=${threads};

inputs=/home/min/a/liu1274/cuda-workspace/FirstPaper/point_corr/input/covtype.7d
#inputs="random"
./knn_bb -v $inputs $npoints
#./knn_bb -v -s $inputs $npoints

