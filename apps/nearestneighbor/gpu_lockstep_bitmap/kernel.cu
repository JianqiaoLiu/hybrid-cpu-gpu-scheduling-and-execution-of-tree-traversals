/* -*- mode: c -*- */

#include <float.h>
#include "nn.h"
#include "nn_gpu.h"
#include "gpu_tree.h"

__global__ void init_kernel(void) {

}

__global__ void nearest_neighbor_search (gpu_tree gpu_tree, gpu_point *d_training_points, int n_training_points,
																				 gpu_point *d_search_points, int n_search_points) 
{

 	float search_points_coord[DIM];
	int closest;
	float closestDist;

#ifdef TRACK_TRAVERSALS
	int numNodesTraversed;
#endif

	int i, j, pidx;
	
	int cur_node_index, prev_node_index;
	__shared__ int SP[NUM_WARPS_PER_BLOCK];
#define sp SP[WARP_IDX]
	
	__shared__ int stk[NUM_WARPS_PER_BLOCK][64];
	int stk_top;

	bool cond, status;
    bool opt1, opt2;
	int critical;
	unsigned int vote_left;
	unsigned int vote_right;
	unsigned int num_left;
	unsigned int num_right;

	gpu_tree_node_0 cur_node0;
	gpu_tree_node_2 cur_node2;
	gpu_tree_node_3 cur_node3;

	float dist=0.0;
	float boxdist=0.0;
	float sum=0.0;
	float boxsum=0.0;
	float center=0.0;
	int id = 0;

#include "nn_kernel_macros.inc"

	for (pidx = blockIdx.x * blockDim.x + threadIdx.x; pidx < n_search_points; pidx += blockDim.x * gridDim.x)
    {
		for(j = 0; j < DIM; j++) {
			search_points_coord[j] = d_search_points[pidx].coord[j];
		}

		closest = d_search_points[pidx].closest;
		closestDist = d_search_points[pidx].closestDist;
#ifdef TRACK_TRAVERSALS
		numNodesTraversed = 0; //d_search_points[pidx].numNodesTraversed;
#endif

		cur_node_index = 0;
		STACK_INIT ();
		STACK_NODE = 0;
		status = 1;
		critical = 63;
		cond = 1;

		while(sp >= 1) {
			cur_node_index = STACK_NODE;

//			if (pidx == 0) {
//				printf("status = %d, critical = %d, sp = %d, and cur_node_index = %d\n", status, critical, sp, cur_node_index);
//			}

			if (status == 0 && critical >= sp) {
				status = 1;
			}
			STACK_POP();

//            if (pidx == 0 && status == 0 ) {
//                printf("FUCK!\n");
//            }
			if (status == 1) {
#ifdef TRACK_TRAVERSALS
			numNodesTraversed++;
#endif
//                critical = sp - 1;
//                if (pidx == 0) {
//                    printf("cur_node_index = %d\n", cur_node_index);
//                }
				//cur_node1 = gpu_tree.nodes1[cur_node_index];
				// inlined function can_correlate
				dist=0.0;
				boxdist=0.0;
				sum=0.0;
				boxsum=0.0;
				center=0.0;

				for(i = 0; i < DIM; i++) {
					float max = gpu_tree.nodes1[cur_node_index].items[i].max;
					float min = gpu_tree.nodes1[cur_node_index].items[i].min;
				    center = (max + min) / 2;
					boxdist = (max - min) / 2;
					dist = search_points_coord[i] - center;
					sum += dist * dist;
					boxsum += boxdist * boxdist;
				}

				cond = (sqrt(sum) - sqrt(boxsum) < sqrt(closestDist));
                critical = sp;
				if(!__any(cond)) {
					continue;
				}

//                critical = sp - 1;
				if (!cond) {
					status = 0;
//                    critical = sp - 1;
				} else {
					cur_node0 = gpu_tree.nodes0[cur_node_index];
					if(cur_node0.items.axis == DIM) {
						cur_node3 = gpu_tree.nodes3[cur_node_index];
						for(i = 0; i < MAX_POINTS_IN_CELL; i++) {
							if(cur_node3.points[i] >= 0) {
								// update closest...
								float dist = 0.0;
								float t;

								for(j = 0; j < DIM; j++) {
									t = (d_training_points[cur_node3.points[i]].coord[j] - search_points_coord[j]);
									dist += t*t;
								}

								if(dist <= closestDist) {
									closest = cur_node3.points[i];
									closestDist = dist;
								}
							}
						}

					} else {
						cur_node2 = gpu_tree.nodes2[cur_node_index];
						opt1 = search_points_coord[cur_node0.items.axis] < cur_node0.items.splitval;
						opt2 = search_points_coord[cur_node0.items.axis] >= cur_node0.items.splitval;
						vote_left = __ballot(opt1);
						vote_right = __ballot(opt2);
						num_left = __popc(vote_left);
						num_right = __popc(vote_right);
						// majority vote
						if (num_left > num_right) {
							if(RIGHT != NULL_NODE) { STACK_PUSH(RIGHT); }
							if(LEFT != NULL_NODE) { STACK_PUSH(LEFT); }
						} else {
							if(LEFT != NULL_NODE) { STACK_PUSH(LEFT); }
							if(RIGHT != NULL_NODE) { STACK_PUSH(RIGHT); }
						}
					}
				}
			}
		}

		d_search_points[pidx].closest = closest;
		d_search_points[pidx].closestDist = closestDist;
#ifdef TRACK_TRAVERSALS
		d_search_points[pidx].numNodesTraversed = numNodesTraversed;
#endif

	}
}
 
