make clean;
make DIM=7 SPLICE_DEPTH=1
./bh ../../../input/bh/BarnesHutPlummer1M.in
./bh ../../../input/bh/BarnesHutRandom1M_same_mass.in

make clean;
make DIM=7 SPLICE_DEPTH=2
./bh ../../../input/bh/BarnesHutPlummer1M.in
./bh ../../../input/bh/BarnesHutRandom1M_same_mass.in

make clean;
make DIM=7 SPLICE_DEPTH=3
./bh ../../../input/bh/BarnesHutPlummer1M.in
./bh ../../../input/bh/BarnesHutRandom1M_same_mass.in

make clean;
make DIM=7 SPLICE_DEPTH=4
./bh ../../../input/bh/BarnesHutPlummer1M.in
./bh ../../../input/bh/BarnesHutRandom1M_same_mass.in

make clean;
make DIM=7 SPLICE_DEPTH=5
./bh ../../../input/bh/BarnesHutPlummer1M.in
./bh ../../../input/bh/BarnesHutRandom1M_same_mass.in

#make clean;
#make DIM=7 SPLICE_DEPTH=6
#./bh ../../../input/pc/covtype.7d 
#./bh ../../../input/pc/mnist.7d 
#./bh ../../../input/pc/random.7d 
#make clean;
#make DIM=2 SPLICE_DEPTH=6
#./bh ../../../input/pc/geocity.txt 

#make clean;
#make DIM=7 SPLICE_DEPTH=7
#./bh ../../../input/pc/covtype.7d 
#./bh ../../../input/pc/mnist.7d 
#./bh ../../../input/pc/random.7d 
#make clean;
#make DIM=2 SPLICE_DEPTH=7
#./bh ../../../input/pc/geocity.txt 

