#! /bin/bash -x

APP=bh
TYPE=Hybrid_Sched
INPUT_DIR=../../../input/bh/
INPUT_FILES=(${INPUT_DIR}BarnesHutPlummer200K.in ${INPUT_DIR}BarnesHutPlummer1M.in ${INPUT_DIR}BarnesHutRandom1M_diff_mass.in ${INPUT_DIR}BarnesHutRandom1M_same_mass.in)
#INPUT_DIM=(3 3 3)
#SPLICE_DEPTH=(4 3 2 2)
#SPLICE_DEPTH=(1 1 1 1)
#WARP_DIR=../../../results/warp_length/${APP}/
TIME_DIR=../../../results/depth_analysis/${APP}/
TMP_DIR=../../../results/depth_tmp/
SCRIPT_DIR=../../../scripts/
NUM_RUNS=15
begin_depth=1
end_depth=(4 3 2 2)
#npoints=200000

MAKE_ARGS=()
for arg in $*
do
    MAKE_ARGS[${#MAKE_ARGS}]=$arg
    if [[ $arg = "TRACK_TRAVERSALS=1" ]]
    then
        NUM_RUNS=1
    fi
done

((j=0))
for file in ${INPUT_FILES[*]}
do
    for ((depth = begin_depth; depth <= ${end_depth[$j]}; depth ++))
    do
        make clean;
        make ${MAKE_ARGS[*]} SPLICE_DEPTH=${depth}
        f=$(basename $file)

        echo "deleting tmp files in app=$APP type=$TYPE input=$f depth=$depth..."
        rm -rf ${TMP_DIR}${APP}_${TYPE}_${f}_${depth}_unsorted.stats
        rm -rf ${TMP_DIR}${APP}_${TYPE}_${f}_${depth}_unsorted.out
        rm -rf ${TIME_DIR}${APP}_${TYPE}_${f}_${depth}_unsorted.time

        for ((i = 0; i < NUM_RUNS; i ++))
        do
            echo "$i th iteration for time test in app=$APP type=$TYPE input=$f depth=$depth..."
            ./bh $file 2>&1 | ${SCRIPT_DIR}collect.py ${TMP_DIR}${APP}_${TYPE}_${f}_${depth}_unsorted.stats 2>&1 >> ${TMP_DIR}${APP}_${TYPE}_${f}_${depth}_unsorted.out 
        done
    
        echo "generating result for time test in app=$APP type=$TYPE input=$f depth=$depth..."
        python ${SCRIPT_DIR}show_results.py ${TMP_DIR}${APP}_${TYPE}_${f}_${depth}_unsorted.stats 2>&1 >> ${TIME_DIR}${APP}_${TYPE}_${f}_${depth}_unsorted.time
    done
    ((j++))
done

