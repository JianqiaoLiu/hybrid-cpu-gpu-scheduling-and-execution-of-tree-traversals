#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include <float.h>
#include <cmath>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <stdint.h>

#include <pthread.h>

#include "bh.h"
#include "util_common.h"

int nthreads;

uint64_t nbodies; // number of bodies (points)
unsigned int ntimesteps; // number of simulation time steps
float dtime; // delta t
float eps; // softening parameter
float tol; // tolerance <= 0.57
float half_dtime; // dtime / 2
float inv_tol_squared; // 1.0 / tol^2
float eps_squared;

vec3d center; // center of the universe
float diameter; // diameter of the universe box

bh_oct_tree_node* points; // array of simulation points
bh_oct_tree_node** points_sorted;   // array of simulation points sorted spatially
bh_oct_tree_node** points_unsorted; // array of simulation points in unsorted order
bh_oct_tree_node** points_array; // pointer to the array of simulation points

uint64_t sortidx;

bh_oct_tree_node* root; // root of the OctTree that represents our universe

int sort_flag, verbose_flag, check_flag;

void read_input(int argc, char **argv) {
	int i, c;
	int junk;

	if(argc < 1 || argc > 7) {
		fprintf(stderr, "Usage: bh [-c] [-v] [-t <nthreads>] [-s] [input_file] [nbodies]\n");
		exit(1);
	}

	check_flag = 0;
	sort_flag = 0;
	verbose_flag = 0;
	nthreads = 1;
	while((c = getopt(argc, argv, "cvt:s")) != -1) {
		switch(c) {
		case 'c':
			check_flag = 1;
			break;

		case 'v':
			verbose_flag = 1;
			break;

		case 't':
			nthreads = atoi(optarg);
			if(nthreads <= 0) {
				fprintf(stderr, "Error: invalid number of threads.\n");
				exit(1);
			}
			break;

		case 's':
			sort_flag = 1;
			break;

		case '?':
			fprintf(stderr, "Error: unknown option.\n");
			exit(1);
			break;

		default:
			abort();
		}
	}

	FILE * infile = stdin;
	nbodies = 0;
	for(i = optind; i < argc; i++) {
		switch(i - optind) {
		case 0:
			infile = fopen(argv[i], "r");
			if(!infile) {
				fprintf(stderr, "Error: could not read input file: %s\n", argv[i]);
				exit(1);
			}
			break;

		case 1:
			nbodies = atoll(argv[i]);
			if(nbodies <= 0) {
				fprintf(stderr, "Error: nbodies not valid.\n");
				exit(1);
			}
			printf("Overriding nbodies from input file. nbodies = %d\n", nbodies);
			fscanf(infile, "%d", &junk); // chomp the input size so
			break;
		}
	}
	
	if(nbodies <= 0) {
	  fscanf(infile, "%lld", &nbodies);
	  if (nbodies < 1) {
			fprintf(stderr, "Error: nbodies must be at least 1!\n");
			exit(1);
	  }
	}

	fscanf(infile, "%d", &ntimesteps);
	if (ntimesteps < 1) {
		fprintf(stderr, "Error: ntimesteps must be at least 1!\n");
		exit(1);
	}

	fscanf(infile, "%f", &dtime);
	if (dtime <= 0.0) {
		fprintf(stderr, "Error: dtime can not be zero!\n");
		exit(1);
	}

	fscanf(infile, "%f", &eps);
	fscanf(infile, "%f", &tol);

	half_dtime = 0.5 * dtime;
	inv_tol_squared = 1.0 / (tol * tol);
	eps_squared = eps * eps;

	/* Read the points */
	//points = (bh_oct_tree_node*) calloc(nbodies, sizeof(bh_oct_tree_node));
	SAFE_CALLOC(points, nbodies, sizeof(bh_oct_tree_node));
	SAFE_CALLOC(points_sorted, nbodies, sizeof(bh_oct_tree_node*));
	SAFE_CALLOC(points_unsorted, nbodies, sizeof(bh_oct_tree_node*));
	sortidx = 0; 

	for (i = 0; i < nbodies; i++) {
		points[i].type = bhLeafNode;

		if (fscanf(infile, "%f %f %f %f %f %f %f", &(points[i].mass),
				&(points[i].cofm.x), &(points[i].cofm.y), &(points[i].cofm.z),
				&(points[i].vel.x), &(points[i].vel.y), &(points[i].vel.z))
				!= 7) {
			fprintf(stderr, "Error: Invalid point (%d).\n", i);
			exit(1);
		}

		points[i].acc.x = points[i].acc.y = points[i].acc.z = 0.0; // no initial acceleration
		clear_children(&(points[i])); // leaf nodes have no children

		points[i].id = i; // mainly to debug and keep track of points
		points_unsorted[i] = &points[i];
	}

	if (infile != stdin) {
		fclose(infile);
	}
}

void compute_bounding_box(void) {
	vec3d min, max, pos;
	int i;

	min.x = min.y = min.z = (float)FLT_MAX;
	max.x = max.y = max.z = (float)FLT_MIN;

	/* compute the max and min positions to form a bounding box */
	for (i = 0; i < nbodies; i++) {
		pos = points[i].cofm;

		if (min.x > pos.x)
			min.x = pos.x;

		if (min.y > pos.y)
			min.y = pos.y;

		if (min.z > pos.z)
			min.z = pos.z;

		if (max.x < pos.x)
			max.x = pos.x;

		if (max.y < pos.y)
			max.y = pos.y;

		if (max.z < pos.z)
			max.z = pos.z;
	}

	/* compute the maximum of the diameters of all axes */
	diameter = max.x - min.x;

	if (diameter < (max.y - min.y))
		diameter = max.y - min.y;

	if (diameter < (max.z - min.z))
		diameter = max.z - min.z;

	/* compute the center point */
	center.x = (max.x + min.x) * 0.5;
	center.y = (max.y + min.y) * 0.5;
  center.z = (max.z + min.z) * 0.5;
}

void insert_point(bh_oct_tree_node *root, bh_oct_tree_node *p, float r, int depth) {
	vec3d offset;

	offset.x = offset.y = offset.z = 0.0;

	CHECK_PTR(root);
	CHECK_PTR(p);

	/*
	 * From the root locate where this point will be:
	 * 		- 0 is lower-front-left
	 * 		- 1 is lower-front-right
	 * 		- 2 is lower-back-left
	 * 		- 3 is lower-back-right
	 * 		- 4 is upper-front-left
	 * 		- 5 is upper-front-right
	 * 		- 6 is upper-back-left
	 * 		- 7 is upper-back-right
	 *
	 * 1. Starting from the lower, front left compare the x component:
	 * 		- if p.x is greater than root.x then go right (add 1)
	 * 		- else stay left
	 *
	 * 		- if p.y is greater than root.y then go back (add 2)
	 * 		- else stay front
	 *
	 * 		- if p.z is greater than root.z then go up (add 4)
	 * 		- else stay low
	 */
	
	// compute this to avoid arithmetic nightmare?
	int space = 0; // to place point
	if (root->cofm.x < p->cofm.x) {
		space = 1;
		offset.x = r;
	}

	if (root->cofm.y < p->cofm.y) {
		space += 2;
		offset.y = r;
	}

	if (root->cofm.z < p->cofm.z) {
		space += 4;
		offset.z = r;
	}

	bh_oct_tree_node * child = root->children[space];
	
	// There was no child here
	if (child == 0) {
		root->children[space] = p;
	} else {

		float half_r = (float)0.5 * r;

		if (child->type == bhLeafNode) {
			// If we reach a leaf node then we must further divide the space
			// Note there is no need to allocate a new node, we will just reuse the
			// current leaf
			bh_oct_tree_node * new_inner_node;
			SAFE_MALLOC(new_inner_node, sizeof(bh_oct_tree_node));

			clear_children(new_inner_node);

			// Calculate the position for the new region based on both points
			// x_new = root_x - (0.5 * r) + offset_x
			// ... same for y and z
			new_inner_node->cofm.x = (root->cofm.x - half_r) + offset.x;
			new_inner_node->cofm.y = (root->cofm.y - half_r) + offset.y;
			new_inner_node->cofm.z = (root->cofm.z - half_r) + offset.z; 
			
			new_inner_node->mass = 0.0;

			new_inner_node->type = bhNonLeafNode;
			new_inner_node->id = -1;

			insert_point(new_inner_node, p, half_r, depth + 1);
			insert_point(new_inner_node, child, half_r, depth + 1);

			root->children[space] = new_inner_node;
		} else {
			insert_point(child, p, half_r, depth + 1);
		}
	}
}

void free_tree(bh_oct_tree_node *root) {
	int i;
	
	CHECK_PTR(root);
	
	for (i = 0; i < 8; i++) {
		if (root->children[i] != 0 && root->children[i]->type == bhNonLeafNode) {
			free_tree(root->children[i]);
			root->children[i] = 0;
		}
	}

	free(root);
}

void compute_center_of_mass(bh_oct_tree_node *root) {
	int i = 0;
	int j = 0;
	float mass;
	vec3d cofm;
	vec3d cofm_child;
	bh_oct_tree_node * child;

	CHECK_PTR(root);

	mass = 0.0;
	cofm.x = cofm.y = cofm.z = 0.0;

	for (i = 0; i < 8; i++) {
		child = root->children[i];
		if (child != 0) {
			// compact child nodes for speed
			if (i != j) {
				root->children[j] = root->children[i];
				root->children[i] = 0;
			}

			j++;

			// If non leave node need to traverse children:
			if (child->type == bhNonLeafNode) {
				// summarize children
				compute_center_of_mass(child);
			} else {
				points_sorted[sortidx++] = child; // insert this point in sorted order
			}

			mass += child->mass;

			cofm.x += child->cofm.x * child->mass;
			cofm.y += child->cofm.y * child->mass;
			cofm.z += child->cofm.z * child->mass;
		}
	}

	root->cofm.x = cofm.x / mass;
	root->cofm.y = cofm.y / mass;
	root->cofm.z = cofm.z / mass;
	root->mass = mass;
}

void compute_force_recursive(bh_oct_tree_node *p, bh_oct_tree_node *root, float dsq, float epssq) {
	vec3d dr;
	bh_oct_tree_node * child;
	float drsq, nphi, scale, idr;
	int i;

	CHECK_PTR(p);
	CHECK_PTR(root);

	dr.x = root->cofm.x - p->cofm.x;
	dr.y = root->cofm.y - p->cofm.y;
	dr.z = root->cofm.z - p->cofm.z;
	drsq = (dr.x * dr.x + dr.y * dr.y + dr.z * dr.z) + epssq;
	
#ifdef TRACK_TRAVERSALS
	p->nodes_accessed++;
#endif
	
	// always do recursive to get most exact
	if (drsq < dsq) {
		if (root->type == bhNonLeafNode) {
			dsq *= 0.25;
			for (i = 0; i < 8; i++) {
				child = root->children[i];
				if (child != 0) {
					compute_force_recursive(p, child, dsq, epssq);
				} else {
					// children are compacted so once we get to a null child we can stop
					break;
				}
			}
		} else {
			if (p != root) // two different particles
			{
				idr = 1.0 / sqrt(drsq);
				nphi = root->mass * idr;
				scale = nphi * idr * idr;

				// other point will affect this points acceleration
				p->acc.x += dr.x*scale;
				p->acc.y += dr.y*scale;
				p->acc.z += dr.z*scale;

			}
		}
	} 
	else // particle is far away can stop here
	{
		idr = 1.0 / sqrt(drsq);
		nphi = root->mass * idr;
		scale = nphi * idr * idr;

		// other point will affect this points acceleration
		p->acc.x += dr.x*scale;
		p->acc.y += dr.y*scale;
		p->acc.z += dr.z*scale;
	}
}

void compute_force(bh_oct_tree_node *p, bh_oct_tree_node *root, float size, float itolsq, int step, float dthf, float epssq) {
	vec3d a_prev;
	
	CHECK_PTR(root);
	CHECK_PTR(p);

	a_prev = p->acc;
	p->acc.x = p->acc.y = p->acc.z = 0.0;

	compute_force_recursive(p, root, size * size * itolsq, epssq);

	if (step > 0) {
		vec3d delta_v;
		delta_v.x = (p->acc.x - a_prev.x) * dthf;
		delta_v.y = (p->acc.y - a_prev.y) * dthf;
		delta_v.z = (p->acc.z - a_prev.z) * dthf;

		p->vel.x += delta_v.x;
		p->vel.y += delta_v.y;
		p->vel.z += delta_v.z;
	}
}

void advance_point(bh_oct_tree_node * p, float dthf, float dtime) {
	vec3d delta_v;
	vec3d velh;
	vec3d velh_by_dt;
	vec3d delta_p;

	CHECK_PTR(p);

	delta_v.x = p->acc.x * dthf;
	delta_v.y = p->acc.y * dthf;
	delta_v.z = p->acc.z * dthf;

	velh.x = (p->vel.x + delta_v.x);
	velh.y = (p->vel.y + delta_v.y);
	velh.z = (p->vel.z + delta_v.z);

	p->cofm.x += velh.x * dtime;
	p->cofm.y += velh.y * dtime;
	p->cofm.z += velh.z * dtime;
	
	p->vel.x = velh.x + delta_v.x;
	p->vel.y = velh.y + delta_v.y;
	p->vel.z = velh.z + delta_v.z;
}

void* call_compute_force(void * args) {
	targs * ta = (targs*)args;
	int i = 0;
	int start = (ta->tid * (nbodies / nthreads));
	int end =(ta->tid + 1) * (nbodies / nthreads);	

	if(end > nbodies)
		end = nbodies;

	//printf("Thread %d start = %d end = %d\n", ta->tid, start, end);
	if(sort_flag)
		points_array = points_sorted;
	else
		points_array = points_unsorted;

	for(i = start;  i < end;  i++) { 
		#ifdef TRACK_TRAVERSALS
		points_array[i]->nodes_accessed = 0;
		#endif
		compute_force(points_array[i], root, diameter, inv_tol_squared, ta->t, half_dtime, eps_squared);
	}

	pthread_exit(NULL);
}

TIME_INIT(overall);
TIME_INIT(read_input);
TIME_INIT(time_step);
TIME_INIT(compute_bounding_box);
TIME_INIT(build_tree);
TIME_INIT(compute_center_of_mass);
TIME_INIT(force_calc);
TIME_INIT(advance_point);

int main(int argc, char **argv) {
	int t, i;

	TIME_START(overall);
	TIME_START(read_input);

	read_input(argc, argv);

	TIME_END(read_input);

	printf("Configuration: sort_flag = %d, verbose_flag = %d, nthreads=%d, nbodies = %d, ntimesteps = %d, dtime = %f eps = %f, tol = %f\n", sort_flag, verbose_flag, nthreads, nbodies, ntimesteps, dtime, eps, tol);

	for (t = 0; t < ntimesteps; t++) {
		printf("Time step %d:\n", t);

		TIME_START(time_step);
		TIME_START(compute_bounding_box);

		compute_bounding_box();
		
		TIME_END(compute_bounding_box);
		
		TIME_START(build_tree);

		SAFE_MALLOC(root, sizeof(bh_oct_tree_node));

		root->id = -1;
		root->type = bhNonLeafNode;
		root->mass = 0.0;

		clear_children(root);

		// Root is never a leaf
		root->cofm = center;
		for (i = 0; i < nbodies; i++) {
			insert_point(root, &(points[i]), diameter * 0.5, 0);
		}

		TIME_END(build_tree);
		TIME_START(compute_center_of_mass);

		// Summarize nodes
		sortidx = 0;
		compute_center_of_mass(root);
// print the position of all points:
        if(verbose_flag) 
        {
            for (i = 0; i < nbodies; i = i + 10000) 
            {
                bh_oct_tree_node * p = points_sorted[i];
                printf("%d, %d: %1.4f %1.4f %1.4f\n", i, p->id, p->cofm.x, p->cofm.y, p->cofm.z);
            }
        }		
		
		TIME_END(compute_center_of_mass);
		TIME_START(force_calc);

		// create a bunck of threads
		int rc;
		pthread_t * threads;
		SAFE_MALLOC(threads, sizeof(pthread_t)*nthreads);

		targs * args;
		SAFE_MALLOC(args, sizeof(targs)*nthreads);

		for(i = 0; i < nthreads; i++) {
			args[i].tid = i;
			args[t].t = t;
			rc = pthread_create(&threads[i], NULL, call_compute_force, &args[i]);
			if(rc) {
				fprintf(stderr, "Error: could not create thread, rc = %d\n", rc);
				exit(1);
			}
		}
		
		// wait for threads
		for(i = 0; i < nthreads; i++) {
			pthread_join(threads[i], NULL);
		}
		
		TIME_END(force_calc);
		TIME_START(advance_point);
		
		for (i = 0; i < nbodies; i++) {
			advance_point(points_sorted[i], half_dtime, dtime);
		}

#ifdef TRACK_TRAVERSALS
		unsigned long long sum_nodes_accessed = 0;
		for(i = 0; i < nbodies; i++) {
			sum_nodes_accessed += points_sorted[i]->nodes_accessed;			
		}
		printf("sum_nodes_accessed: %llu\n", sum_nodes_accessed);
		printf("avg_nodes_accessed: %f\n", (float)sum_nodes_accessed / nbodies);
#endif
		
		TIME_END(advance_point);
		
		// Cleanup before the next run
		free_tree(root);
		root = 0;

		TIME_END(time_step);
		
		TIME_ELAPSED_PRINT(build_tree, stdout);
		TIME_ELAPSED_PRINT(compute_center_of_mass, stdout);
		TIME_ELAPSED_PRINT(force_calc, stdout);
		TIME_ELAPSED_PRINT(advance_point, stdout);
	}

	TIME_END(overall);
	
	TIME_ELAPSED_PRINT(read_input, stdout);
	TIME_ELAPSED_PRINT(overall, stdout);

	// print the position of all points:
/*	if(verbose_flag) {
		for (i = 0; i < nbodies; i = i + 10000) {
			bh_oct_tree_node * p = points_sorted[i];
			printf("%d, %d: %1.4f %1.4f %1.4f\n", i, p->id, p->cofm.x, p->cofm.y, p->cofm.z);
		}
	}
*/	
	pthread_exit(NULL);
	return 0;
}
