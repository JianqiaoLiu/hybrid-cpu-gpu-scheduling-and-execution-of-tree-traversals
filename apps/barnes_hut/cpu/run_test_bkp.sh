#! /bin/sh -x

INPUT_SIZE=1000000
INPUT_FILES=(../input/BarnesHutC.in)
#INPUT_FILES=($INPUT_DIR/BarnesHutC.in $INPUT_DIR/BarnesHutRand1M.in)
#NUM_RUNS=10
#THREADS=(32 24 20 16 12 8 4 2 1)
THREADS=1

MAKE_ARGS=()

make clean
make 
#   ./bh -t $t $file 2>&1 | collect.py ${f}_$host_${t}.stats 2>&1 >>${f}_$host_${t}.out
    ./bh -s -v -t $THREADS $INPUT_FILES 2>&1
#    ./bh -s -v -t $THREADS $INPUT_FILES 2>&1
