#ifdef USE_SMEM
#define STACK_INIT() sp = 1; stack[sp][WARP_INDEX].items.index = 0; stack[sp][WARP_INDEX].items.dsq = size * size * itolsq
#define STACK_POP() sp -= 1
#define STACK_PUSH() sp += 1
#define STACK_TOP_NODE_INDEX stack[sp][WARP_INDEX].items.index
#define STACK_TOP_DSQ stack[sp][WARP_INDEX].items.dsq
#else
//#define STACK_INIT() sp = 1; stack_node_index[sp] = 0; stack_dsq[sp] = base_dsq
#define STACK_INIT() sp = 1; stack_node_index[sp] = 0; stack_dsq[sp] = size * size * itolsq
#define STACK_POP() sp -= 1
#define STACK_PUSH() sp += 1
#define STACK_TOP_NODE_INDEX stack_node_index[sp]
#define STACK_TOP_DSQ stack_dsq[sp]
#endif

#define CUR_NODE0 cur_node0[WARP_INDEX]
#define CUR_NODE1 cur_node1[WARP_INDEX]
#define CUR_NODE2 cur_node2[WARP_INDEX]
#define CUR_NODE3 cur_node3[WARP_INDEX]
