#! /bin/bash -x

APP=bh
TYPE=cgpu
INPUT_DIR=../../../input/bh/
INPUT_FILES=(${INPUT_DIR}BarnesHutPlummer200K.in ${INPUT_DIR}BarnesHutPlummer1M.in ${INPUT_DIR}BarnesHutRandom1M_diff_mass.in ${INPUT_DIR}BarnesHutRandom1M_same_mass.in)
#INPUT_DIM=(3 3 3)
SPLICE_DEPTH=(4 3 3 3)
WARP_DIR=../../../results/warp_length/${APP}/
TIME_DIR=../../../results/runtime_35/${APP}/
TMP_DIR=../../../results/tmp/
SCRIPT_DIR=../../../scripts/
NUM_RUNS=15
#npoints=200000

MAKE_ARGS=()
for arg in $*
do
    MAKE_ARGS[${#MAKE_ARGS}]=$arg
    if [[ $arg = "TRACK_TRAVERSALS=1" ]]
    then
        NUM_RUNS=1
    fi
done

((j=0))
for file in ${INPUT_FILES[*]}
do
    make clean;
    make ${MAKE_ARGS[*]} SPLICE_DEPTH=${SPLICE_DEPTH[$j]}
    f=$(basename $file)

    echo "deleting tmp files in app=$APP type=$TYPE input=$f..."
    rm -rf ${TMP_DIR}${APP}_${TYPE}_${f}_unsorted.stats
    rm -rf ${TMP_DIR}${APP}_${TYPE}_${f}_sorted.stats
    rm -rf ${TMP_DIR}${APP}_${TYPE}_${f}_unsorted.out
    rm -rf ${TMP_DIR}${APP}_${TYPE}_${f}_sorted.out
    rm -rf ${TIME_DIR}${APP}_${TYPE}_${f}_unsorted.time
    rm -rf ${TIME_DIR}${APP}_${TYPE}_${f}_sorted.time
    rm -rf ${WARP_DIR}${APP}_${TYPE}_${f}_unsorted.warp
    rm -rf ${WARP_DIR}${APP}_${TYPE}_${f}_sorted.warp

    for ((i = 0; i < NUM_RUNS; i ++))
    do
        echo "$i th iteration for time test in app=$APP type=$TYPE input=$f..."
        ./bh $file 2>&1 | ${SCRIPT_DIR}collect.py ${TMP_DIR}${APP}_${TYPE}_${f}_unsorted.stats 2>&1 >> ${TMP_DIR}${APP}_${TYPE}_${f}_unsorted.out 
        ./bh -s $file 2>&1 | ${SCRIPT_DIR}collect.py ${TMP_DIR}${APP}_${TYPE}_${f}_sorted.stats 2>&1 >> ${TMP_DIR}${APP}_${TYPE}_${f}_sorted.out
    done
    
    echo "generating result for time test in app=$APP type=$TYPE input=$f..."
    python ${SCRIPT_DIR}show_results.py ${TMP_DIR}${APP}_${TYPE}_${f}_unsorted.stats 2>&1 >> ${TIME_DIR}${APP}_${TYPE}_${f}_unsorted.time
    python ${SCRIPT_DIR}show_results.py ${TMP_DIR}${APP}_${TYPE}_${f}_sorted.stats 2>&1 >> ${TIME_DIR}${APP}_${TYPE}_${f}_sorted.time

    make clean;
    make ${MAKE_ARGS[*]} SPLICE_DEPTH=${SPLICE_DEPTH[$j]} TRACK_TRAVERSALS=1

    echo "executing for warp test in app=$APP type=$TYPE input=$f..."
    ./bh -w $file > ${WARP_DIR}${APP}_${TYPE}_${f}_unsorted.warp  
    ./bh -s -w $file > ${WARP_DIR}${APP}_${TYPE}_${f}_sorted.warp

    ((j++))
done

