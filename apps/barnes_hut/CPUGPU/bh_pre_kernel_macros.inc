#ifndef USE_LOCAL_STACK
#define STACK_INIT() sp = 1; \
    stack_node_index = &params.root.stk_nodes[params.root.depth*8*blockIdx.x*blockDim.x + threadIdx.x]; \
    stack_dsq = &params.root.stk_dsq[params.root.depth*8*blockIdx.x*blockDim.x + threadIdx.x]; \
    *stack_node_index = 0; *stack_dsq = size * size * itolsq

#define STACK_POP() sp -= 1; stack_node_index -= blockDim.x; stack_dsq -= blockDim.x
#define STACK_PUSH() sp += 1; stack_node_index += blockDim.x; stack_dsq += blockDim.x
#define STACK_TOP_NODE_INDEX (*stack_node_index)
#define STACK_TOP_DSQ (*stack_dsq)
#else
#define STACK_INIT() sp = 1; stack_node_index[sp] = 0; stack_dsq[sp] = size * size * itolsq
#define STACK_POP() sp -= 1
#define STACK_PUSH() sp += 1
#define STACK_TOP_NODE_INDEX stack_node_index[sp]
#define STACK_TOP_DSQ stack_dsq[sp]
#endif

#define CUR_NODE0 cur_node0
#define CUR_NODE1 cur_node1
#define CUR_NODE2 cur_node2
#define CUR_NODE3 cur_node3
