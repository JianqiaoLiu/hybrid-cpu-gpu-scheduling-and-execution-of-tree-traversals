#include "splice.h"

SpliceNode::SpliceNode()
{
}

SpliceNode::~SpliceNode()
{
}

const int max_children = 2;
const int max_phases = 2;

int SpliceSet::depth_param;
int SpliceSet::depth_param_half;
int SpliceSet::num_leaf_nodes;
int SpliceSet::num_nodes;

int SpliceSet::int_exp(int n)
{
	int ret = 1;
	while (n-- > 0)
    {
		ret *= max_children;
	}
	return ret;
}

void SpliceSet::init(int d)
{
//	assert(d > 0);
	depth_param = d;
	depth_param_half = depth_param >> 1;
	num_leaf_nodes = int_exp(depth_param + 1);
	num_nodes = num_leaf_nodes * max_children;
	assert(num_nodes > num_leaf_nodes);	// for very large d this could overflow
}

SpliceSet::SpliceSet()
{
	splice_nodes_src = new SpliceNode*[depth_param + 2];
	splice_nodes_dest = new SpliceNode*[depth_param + 2];
	for (int i = 0; i < depth_param + 2; i++)
    {
		splice_nodes_src[i] = NULL;
		splice_nodes_dest[i] = NULL;
	}
	tree_map = new kd_cell*[num_nodes];
}

SpliceSet::~SpliceSet()
{
	for (int i = 0; i < depth_param + 2; i++)
    {
		if (splice_nodes_src[i] == NULL)
            continue;
		delete splice_nodes_src[i];
		delete splice_nodes_dest[i];
	}
	delete [] splice_nodes_src;
	delete [] splice_nodes_dest;
	delete [] tree_map;
}

void SpliceSet::map_tree(int depth, int index, kd_cell *node)
{
	assert(index < num_nodes);
	tree_map[index] = node;
	// for splice set stacks, can be init multiple times
	if (splice_nodes_dest[depth] != NULL)
        return;
	splice_nodes_dest[depth] = new SpliceNode();
	splice_nodes_src[depth] = new SpliceNode();
}

void SpliceSet::swap_nodes(int i)
{
	assert(i < depth_param + 2);
	SpliceNode *swap = splice_nodes_src[i];
	splice_nodes_src[i] = splice_nodes_dest[i];
	splice_nodes_dest[i] = swap;
}

void SpliceSet::do_splice()
{
	phase_infix(0, 1, 0, 1);
}

void SpliceSet::phase_infix(int depth1, int phase1, int depth2, int phase2)
{
	if (depth2 < depth_param)
    {
		for (int i = 0; i < max_children; i++)
        {
			if (i == 0)
            {
				if (tree_map[phase2 * max_children] != NULL)
                {
					phase_infix(depth1, phase1, depth2 + 1, phase2 * max_children);
				}
                else
                {
					top_phase(depth1, phase1);
				}
			}
            else
            {
				if (tree_map[phase2 * max_children + i] != NULL)
                {
						phase_infix(depth2 + 1, phase2 * max_children + i, depth2 + 1, phase2 * max_children + i);
				}
			}
		}
	}
    else
    {
		top_phase(depth1, phase1);
		if (tree_map[phase2 * max_children] != NULL)
        {
			bottom_phase(phase2 * max_children);
		}
	}
}


void SpliceSet::top_phase(int depth, int node_index)
{
	if (depth == 0)
        return;
    
	for (int i = depth; i <= depth_param + 1; i++)
    {
		if (splice_nodes_dest[i] == NULL)
            continue;
		swap_nodes(i);
		SpliceNode *dest = splice_nodes_dest[i];
		dest->clear();
	}
    
	SpliceNode *targetsn = splice_nodes_src[depth];
	for (int i = depth; i <= depth_param + 1; i++)
    {
		SpliceNode *sn = splice_nodes_src[i];
		for (vector<kd_cell *>::iterator it = sn->points.begin() ; it < sn->points.end(); it++)
        {
			process_top_phase(*it, tree_map[node_index], depth);
		}
	}
}

void SpliceSet::bottom_phase(const int start_node_index)
{
	SpliceNode *sn = splice_nodes_dest[depth_param + 1];
	if (sn == NULL)
        return;
	int target = start_node_index;
	int target_rem = target % max_children;
	int target_base = target - target_rem;
    do_bottom_phase(sn, target_base);
//    printf("the splice node contains %d points.\n", sn->points.size());
/*cam	for (vector<kd_cell *>::iterator it = sn->points.begin() ; it < sn->points.end(); it++)
    {
		do_bottom_phase(*it, target_base, sn);
	}*/
}
/*cam
void SpliceSet::do_bottom_phase(kd_cell *point, int target_base, SpliceNode *sn)
{
	process_bottom_phase(point, tree_map[target_base + 0]);
	process_bottom_phase(point, tree_map[target_base + 1]);
}*/

void SpliceSet::do_bottom_phase(SpliceNode *sn, int target_base)
{
    process_bottom_phase(sn, target_base + 0);
    process_bottom_phase(sn, target_base + 1);
}

void correlation_search(SpliceNode *sn, int root_index);
void correlation_search_splice(kd_cell *point, kd_cell *node, int _depth, SpliceSet *_set);

void SpliceSet::process_top_phase(kd_cell *point, kd_cell *node, int depth)
{
	if (node == NULL)
        return;
	correlation_search_splice(point, node, depth, this);
}
/*cam
void SpliceSet::process_bottom_phase(kd_cell *point, kd_cell *node)
{
	if (node == NULL)
        return;
	correlation_search(point, node);
}*/

void SpliceSet::process_bottom_phase(SpliceNode *sn, int node_index)
{
    if (tree_map[node_index] == NULL)
    {
        return;
    }
    correlation_search(sn, node_index);
}













