/*
 * common.h
 *
 *  Created on: Jul 8, 2012
 *      Author: yjo
 */

#ifndef __COMMON_H_
#define __COMMON_H_

#include <stdint.h>

#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cfloat>
#include <cmath>

#include <vector>
#include <memory>
#include <string>
#include <iostream>

using namespace std;

#endif /* __COMMON_H_ */
