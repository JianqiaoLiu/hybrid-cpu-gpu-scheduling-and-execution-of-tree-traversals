//#define OVERLAPPING

#ifdef OVERLAPPING
#define N_STREAMS 4
#endif

#include "common.h"
#include "util_common.h"
#include "data_types.h"
#include "splice.h"
#include "pre_point_corr_gpu.h"
#include "point_corr_gpu.h"
#include "gpu_mem.h"
#include "point_corr.h"

int npoints = 0; // number of input points
kd_cell **points = NULL; // input points
kd_cell *root = NULL; // root of the tree

unsigned long corr_sum = 0;
unsigned int sum_of_nodes = 0;

#ifdef TRACK_TRAVERSALS
unsigned long long nodes_accessed_sum = 0;
unsigned long long nodes_needed_sum = 0;
#endif

gpu_tree *h_root = NULL; // root of the host GPU tree
gpu_tree d_root;

gpu_tree *h_pre_root = NULL; // root of the pre GPU tree
gpu_tree d_pre_root;

bool *h_correlation_matrix;
bool *d_correlation_matrix;
unsigned int COLS = 0;
unsigned int ROWS = 0;

int sort_split = 0; // axis component compared
int sortidx = 0;

TIME_INIT(overall);
TIME_INIT(read_input);
TIME_INIT(cpu_tree);
TIME_INIT(gpu_tree);
TIME_INIT(init_kernel);
TIME_INIT(find_correlation);
TIME_INIT(cpy_cpu_to_gpu);
TIME_INIT(gpu_kernel);
TIME_INIT(cpy_gpu_to_cpu);

TIME_INIT(pre_cpu_tree);
TIME_INIT(pre_gpu_tree);
TIME_INIT(create_matrix);
TIME_INIT(pre_cpy_cpu_to_gpu);
TIME_INIT(pre_gpu_kernel);
TIME_INIT(pre_cpy_gpu_to_cpu);
TIME_INIT(pre_all);
TIME_INIT(other_cpy);

TIME_INIT(single_run);

int main(int argc, char* argv[]) 
{
#ifdef TRACK_TRAVERSALS
//    cudaDeviceProp prop;
//    cudaGetDeviceProperties(&prop, 0);
//    printf("Device : %s. The splice depth is %d.\n", prop.name, SPLICE_DEPTH);
#endif
	int i = 0; // loop variable
	int j = 0; // loop variable

	srand(0); // for quicksort

	TIME_START(overall);

	TIME_START(read_input);
	read_input(argc, argv);
	TIME_END(read_input);
	
	TIME_START(cpu_tree);
	root = build_tree(points, 0, 0, npoints - 1, 1);
	TIME_END(cpu_tree);
	
    srand(0);
    for (i = 0; i < npoints; i ++)
    {
        j = rand() % npoints;
        kd_cell *temp = points[i];
        points[i] = points[j];
        points[j] = temp;
    }

    TIME_START(init_kernel);
    init_kernel<<<1, 1>>>();
    TIME_END(init_kernel);

// Pre compute correlation matrix	
	TIME_START(pre_all);
	TIME_START(pre_gpu_tree);
	h_pre_root = build_pre_gpu_tree(root); // build up the pre gpu tree
    alloc_tree_dev(h_pre_root, &d_pre_root);
	copy_tree_to_dev(h_pre_root, &d_pre_root);
	TIME_END(pre_gpu_tree);

    ROWS = npoints;
    COLS = h_pre_root->max_nnodes;
    int nMatrixSize = ROWS * COLS;
	TIME_START(create_matrix);
	SAFE_MALLOC(h_correlation_matrix, sizeof(bool) * nMatrixSize);  
    CUDA_SAFE_CALL(cudaMalloc(&(d_correlation_matrix), sizeof(bool) * nMatrixSize));
	TIME_END(create_matrix);

	TIME_START(other_cpy);
	gpu_point_set *h_set = NULL; // root of the host GPU point set
    SAFE_MALLOC(h_set, sizeof(gpu_point_set));
    h_set->npoints = npoints;
    SAFE_MALLOC(h_set->nodes0, sizeof(gpu_node0) * npoints);
    SAFE_MALLOC(h_set->nodes3, sizeof(gpu_node3) * npoints);
    for (i = 0 ; i < h_set->npoints; i++)
    {
#ifdef TRACK_TRAVERSALS
        h_set->nodes0[i].nodes_accessed = points[i]->nodes_accessed;
        h_set->nodes0[i].nodes_needed = points[i]->nodes_needed;
#endif
        for (j = 0; j < DIM; j ++)
        {
            h_set->nodes0[i].coord_max[j] = points[i]->coord_max[j];
            h_set->nodes0[i].min[j] = points[i]->min[j];
        }

        h_set->nodes3[i].corr = points[i]->corr;
//        h_set->nodes3[i].cpu_addr = NULL;
		h_set->nodes3[i].point_id = points[i]->id;
    }
	TIME_END(other_cpy);

    gpu_point_set d_set;
    d_set.npoints = h_set->npoints;
	TIME_START(pre_cpy_cpu_to_gpu);
    alloc_set_dev(h_set, &d_set);
    copy_set_to_dev(h_set, &d_set);
	TIME_END(pre_cpy_cpu_to_gpu);
	
	pc_pre_kernel_params d_params;
    d_params.tree = d_pre_root;
	d_params.root_index = 1;
    d_params.set = d_set;
	d_params.relation_matrix = d_correlation_matrix;
    d_params.rad = RADIUS;
    d_params.npoints = d_set.npoints;
	d_params.tree_max_nnodes = h_pre_root->max_nnodes;

    cudaError_t e;
    dim3 tpb(THREADS_PER_BLOCK);
    dim3 nb(NUM_THREAD_BLOCKS);

    // ** Pre Kernel ** //
	TIME_START(pre_gpu_kernel);
	pre_compute_correlation<<<nb, tpb>>>(d_params);
    cudaThreadSynchronize();
	TIME_END(pre_gpu_kernel);
    e = cudaGetLastError();
    if (e != cudaSuccess)
    {
        fprintf(stderr, "error: kernel error: %s.\n", cudaGetErrorString(e));
        exit(1);
    }
    
	TIME_START(pre_cpy_gpu_to_cpu);
    copy_set_to_host(h_set, &d_set);
	CUDA_SAFE_CALL(cudaMemcpy(h_correlation_matrix, d_correlation_matrix, sizeof(bool) * nMatrixSize, cudaMemcpyDeviceToHost));
	TIME_END(pre_cpy_gpu_to_cpu);

    CUDA_SAFE_CALL(cudaFree(d_correlation_matrix));
    d_correlation_matrix = NULL;
    free_set_dev(&d_set);
	free_pre_gpu_tree(h_pre_root);
	free_tree_dev(&d_pre_root);

/*    for (i = 0; i < h_set->npoints; i ++)
    {
    #ifdef TRACK_TRAVERSALS
        points[i]->nodes_accessed = h_set->nodes0[i].nodes_accessed;
        points[i]->nodes_needed = h_set->nodes0[i].nodes_needed;
    #endif
        points[i]->corr = h_set->nodes3[i].corr;
		points[i]->id = h_set->nodes3[i].point_id;
    }*/

    free(h_set->nodes0);
    free(h_set->nodes3);
    free(h_set);
    h_set = NULL;

    TIME_END(pre_all);
//    fprintf(stderr, "the first step!\n");

	TIME_START(gpu_tree);
    h_root = build_gpu_tree(root); // build up the gpu tree
    d_root.nnodes = h_root->nnodes;
	d_root.tree_depth = h_root->tree_depth;
    alloc_tree_dev(h_root, &d_root);
    copy_tree_to_dev(h_root, &d_root);
    TIME_END(gpu_tree);

    TIME_START(find_correlation);
    SpliceSet::init(SPLICE_DEPTH);
    find_correlation(0, npoints);
//    find_correlation(0, 1);
    TIME_END(find_correlation);
    
//    fprintf(stderr, "the second step!\n");   
 
    for(i = 0; i < npoints; i++)
	{
		corr_sum += (unsigned long)points[i]->corr;
#ifdef TRACK_TRAVERSALS
		nodes_accessed_sum += (unsigned long)points[i]->nodes_accessed;
		nodes_needed_sum += (unsigned long)points[i]->nodes_needed;
#endif
	}
    printf("@ avg_corr: %f\n", (float)corr_sum / npoints);
#ifdef TRACK_TRAVERSALS
	printf("@ sum_accessed: %ld\n", nodes_accessed_sum);
	printf("@ sum_needed: %ld\n", nodes_needed_sum);
	printf("@ ratio: %f\n", (float)nodes_needed_sum / nodes_accessed_sum);
#endif
	
    free_tree_dev(&d_root);
    free_gpu_tree(h_root);
    h_root = NULL;
//	free(h_correlation_matrix);

    TIME_END(overall);
 
    TIME_ELAPSED_PRINT(overall, stdout);
    TIME_ELAPSED_PRINT(read_input, stdout);
    TIME_ELAPSED_PRINT(cpu_tree, stdout);
    TIME_ELAPSED_PRINT(gpu_tree, stdout);
    TIME_ELAPSED_PRINT(init_kernel, stdout);    
    TIME_ELAPSED_PRINT(find_correlation, stdout);
    TIME_ELAPSED_PRINT(cpy_cpu_to_gpu, stdout);
    TIME_ELAPSED_PRINT(gpu_kernel, stdout);
    TIME_ELAPSED_PRINT(cpy_gpu_to_cpu, stdout);

	TIME_ELAPSED_PRINT(pre_cpu_tree, stdout);
	TIME_ELAPSED_PRINT(pre_gpu_tree, stdout);
	TIME_ELAPSED_PRINT(create_matrix, stdout);
	TIME_ELAPSED_PRINT(pre_cpy_cpu_to_gpu, stdout);
	TIME_ELAPSED_PRINT(pre_gpu_kernel, stdout);
	TIME_ELAPSED_PRINT(pre_cpy_gpu_to_cpu, stdout);
	TIME_ELAPSED_PRINT(pre_all, stdout);
	TIME_ELAPSED_PRINT(other_cpy, stdout);

    float cpu_kernel_elapsed = find_correlation_elapsed - cpy_cpu_to_gpu_elapsed;
    cpu_kernel_elapsed -= gpu_kernel_elapsed;
    cpu_kernel_elapsed -= cpy_gpu_to_cpu_elapsed;
    fprintf(stdout, "@ cpu_kernel: %2.0f ms\n", cpu_kernel_elapsed);
}

void read_input(int argc, char *argv[]) 
{
	unsigned long long i = 0;
	unsigned long long j = 0;
	int label = 0;
	FILE *in;

	if(argc != 2 && argc != 3) 
	{
		fprintf(stderr, "usage: nn <npoints> [input_file]\n");
		exit(1);
	}

	npoints = atol(argv[1]);
	if(npoints <= 0) 
	{
		fprintf(stderr, "Not enough points.\n");
		exit(1);
	}

	// initialize the points	
	SAFE_MALLOC(points, sizeof(kd_cell*)*npoints);
	
	for(i = 0; i < npoints; i ++) 
	{
		SAFE_MALLOC(points[i], sizeof(kd_cell));
		points[i]->splitType = SPLIT_LEAF;
		points[i]->left = NULL;
		points[i]->right = NULL;
		points[i]->corr = 0;
		#ifdef TRACK_TRAVERSALS
		points[i]->nodes_accessed = 0;
		points[i]->nodes_needed = 0;
		#endif
		points[i]->id = i;
		for(j = 0; j < DIM; j ++) 
		{
			points[i]->coord_max[j] = FLT_MIN;
//			points[i]->coord_max[j] = -1000000.0;
			points[i]->min[j] = FLT_MAX;
		}
	}
	
	if(argc == 3) 
	{
		in = fopen(argv[2], "r");
		if(in == NULL) 
		{
			fprintf(stderr, "Could not open %s\n", argv[4]);
			exit(1);
		}		

		for(i = 0; i < npoints; i++) 
		{
			if(fscanf(in, "%d", &label) != 1)
			{
				fprintf(stderr, "Input file not large enough.\n");
				exit(1);
			}
			for(j = 0; j < DIM; j++) 
			{
				if(fscanf(in, "%f", &(points[i]->coord_max[j])) != 1) 
				{
					fprintf(stderr, "Error: Invalid point %d\n", i);
					exit(1);
				}
				points[i]->min[j] = points[i]->coord_max[j];
			}
			points[i]->id = i;
		}
		if(in != stdin) 
		{
			fclose(in);
		}
	}
	else 
	{
		srand(0);
		for(i = 0; i < npoints; i++)
		{
			points[i]->id = i;
			for(j = 0; j < DIM; j++) 
			{
				points[i]->coord_max[j] = (float)rand() / RAND_MAX;
				points[i]->min[j] = points[i]->coord_max[j];
			}
		}
	}

}

kd_cell * build_tree(kd_cell ** points, int split, int lb, int ub, int index)
{
	int mid = 0;
	int i = 0;
	kd_cell *node = NULL;
	sum_of_nodes ++;	

	if(lb > ub)
		return 0;
	if(lb == ub)
	{
//		return points[lb];
		SAFE_MALLOC(node, sizeof(kd_cell));
		node->splitType = SPLIT_LEAF;
#ifdef TRACK_TRAVERSALS
		node->nodes_accessed = 0;
		node->nodes_needed = 0;
#endif
		node->id = index;
		for(i = 0; i < DIM; i ++)
		{
			node->coord_max[i] = points[lb]->coord_max[i];
			node->min[i] = points[lb]->min[i];
		}
		node->corr = 0;
		node->left = NULL;
		node->right = NULL;
		return node;
	}
	else
	{
		sort_split = split;
		qsort(&points[lb], ub - lb + 1, sizeof(kd_cell*), kdnode_cmp);
		mid = (ub + lb) / 2;
		
		// create a new node to contains the points:
		SAFE_MALLOC(node, sizeof(kd_cell));
		node->splitType = split;
#ifdef TRACK_TRAVERSALS
		node->nodes_accessed = 0;
		node->nodes_needed = 0;
#endif
		node->id = index;
		node->left = build_tree(points, (split + 1) % DIM, lb, mid, 2 * index);
		node->right = build_tree(points, (split + 1) % DIM, mid + 1, ub, 2 * index + 1);
		node->corr = 0;
		
		for(i = 0; i < DIM; i ++)
		{
			node->min[i] = FLT_MAX;
//			node->coord_max[i] = -1000000.0;
			node->coord_max[i] = FLT_MIN;
		}

		if(node->left != NULL) 
		{
			for(i = 0; i < DIM; i++) 
			{
				if(node->coord_max[i] < node->left->coord_max[i]) 
				{
					node->coord_max[i] = node->left->coord_max[i];
				}

				if(node->min[i] > node->left->min[i]) 
				{
					node->min[i] = node->left->min[i];
				}
			}
		}

		if(node->right != NULL) 
		{
			for(i = 0; i < DIM; i++) 
			{
				if(node->coord_max[i] < node->right->coord_max[i]) 
				{
					node->coord_max[i] = node->right->coord_max[i];
				}
				if(node->min[i] > node->right->min[i]) 
				{
					node->min[i] = node->right->min[i];
				}
			}
		}

		return node;
	}

}

int kdnode_cmp(const void *a, const void *b)
{
	/* note: sort split must be updated before call to qsort */
	kd_cell **na = (kd_cell**)a;
	kd_cell **nb = (kd_cell**)b;
	
	if((*na)->coord_max[sort_split] < (*nb)->coord_max[sort_split])
	{
		return -1;
	}
	else if((*na)->coord_max[sort_split] > (*nb)->coord_max[sort_split])
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void free_tree(kd_cell *root)
{
	if(root->left != NULL)
		free_tree(root->left);
	if(root->right != NULL)
		free_tree(root->right);
	delete root;
}

static inline float distance_axis(kd_cell *a, kd_cell *b, int axis)
{
    return (a->coord_max[axis] - b->coord_max[axis]) * (a->coord_max[axis] - b->coord_max[axis]);
}

static inline float distance(kd_cell *a, kd_cell *b)
{
    float d = 0;
    int i = 0;
    for (i = 0; i < DIM; i ++)
    {
        d += distance_axis(a, b, i);
    }
    return d;
}

void find_correlation(int start, int end)
{
    int i = 0;
    auto_ptr<SpliceSet> _set(new SpliceSet);
    correlation_search_map_tree(root, 0, 1, _set.get());
    for (i = start; i < end; i ++)
    {
        correlation_search_splice(points[i], root, 0, _set.get());
    }
    _set->do_splice();
}

void correlation_search_map_tree(kd_cell *node, int _depth, int _index, SpliceSet *_set)
{
    assert(node != NULL);
    
    if (_depth > (SPLICE_DEPTH + 1))
        return;
    _set->map_tree(_depth, _index, node);
    if (node->splitType == SPLIT_LEAF)
    {
        return;
    }
    else
    {
        correlation_search_map_tree(node->left, _depth + 1, _index * 2, _set);
        correlation_search_map_tree(node->right, _depth + 1, _index * 2 + 1, _set);
    }
}

void correlation_search_splice(kd_cell *point, kd_cell *node, int _depth, SpliceSet *_set)
{
    assert(node != NULL);
    
    bool _point_stopped = true;
    SpliceNode *_sn = _set->get_node(_depth);
    
//    if (!can_correlate(point, node))
	if(!h_correlation_matrix[(node->id - 1) * ROWS+ point->id])
	{
        if (_point_stopped)
        {
            _sn->add(point);
		}
        return;
    }
	
    if (node->splitType == SPLIT_LEAF)
    {
/*       float dist = distance(point, node);
       if (sqrt(dist) < RADIUS)
        {
            point->corr ++;
        }*/
	}
    else
    {
        if (_depth < SPLICE_DEPTH)
        {
            correlation_search_splice(point, node->left, _depth + 1, _set);
        }
        else
        {
            _set->get_node(_depth + 1)->add(point);
        }
        _point_stopped = false;
    }
    if (_point_stopped)
    {
        _sn->add(point);
    }
}

void correlation_search(SpliceNode *sn, int root_index)
{
    int i = 0;
    int j = 0;
    // build the gpu traversal point set manually
    gpu_point_set *h_set = NULL; // root of the host GPU point set
    SAFE_MALLOC(h_set, sizeof(gpu_point_set));
    h_set->npoints = sn->points.size();
    SAFE_MALLOC(h_set->nodes0, sizeof(gpu_node0) * h_set->npoints);
    SAFE_MALLOC(h_set->nodes3, sizeof(gpu_node3) * h_set->npoints);
    for (i = 0 ; i < h_set->npoints; i++)
    {
#ifdef TRACK_TRAVERSALS
        h_set->nodes0[i].nodes_accessed = sn->points[i]->nodes_accessed;
        h_set->nodes0[i].nodes_needed = sn->points[i]->nodes_needed;
#endif
        for (j = 0; j < DIM; j ++)
        {
            h_set->nodes0[i].coord_max[j] = sn->points[i]->coord_max[j];
            h_set->nodes0[i].min[j] = sn->points[i]->min[j];
        }
        
        h_set->nodes3[i].corr = sn->points[i]->corr;
//        h_set->nodes3[i].cpu_addr = NULL;
		h_set->nodes3[i].point_id = sn->points[i]->id;
	}

    gpu_point_set d_set;
    d_set.npoints = h_set->npoints;
    TIME_RESTART(cpy_cpu_to_gpu);
    alloc_set_dev(h_set, &d_set);
    copy_set_to_dev(h_set, &d_set);
    TIME_END(cpy_cpu_to_gpu);

    pc_kernel_params d_params;
    d_params.tree = d_root;
    d_params.root_index = root_index;
    d_params.set = d_set;
    d_params.rad = RADIUS;
    d_params.npoints = d_set.npoints;

    cudaError_t e;
    dim3 tpb(THREADS_PER_BLOCK);
    dim3 nb(NUM_THREAD_BLOCKS);

    // ** Kernel ** //
    TIME_RESTART(gpu_kernel);
	TIME_START(single_run);
    compute_correlation<<<nb, tpb>>>(d_params);
    cudaThreadSynchronize();
    TIME_END(gpu_kernel);
	TIME_END(single_run);
//	TIME_ELAPSED_PRINT(single_run, stdout);
    e = cudaGetLastError();
    if (e != cudaSuccess)
    {
        fprintf(stderr, "error: kernel error: %s.\n", cudaGetErrorString(e));
        exit(1);
    }
    
    // Kernel clean up
    TIME_RESTART(cpy_gpu_to_cpu);
    copy_set_to_host(h_set, &d_set);
    TIME_END(cpy_gpu_to_cpu);

    free_set_dev(&d_set);

    // Copy back
    for (i = 0; i < h_set->npoints; i ++)
    {
    #ifdef TRACK_TRAVERSALS
        sn->points[i]->nodes_accessed = h_set->nodes0[i].nodes_accessed;
        sn->points[i]->nodes_needed = h_set->nodes0[i].nodes_needed;
    #endif
        sn->points[i]->corr = h_set->nodes3[i].corr;
    }

    free(h_set->nodes0);
    free(h_set->nodes3);
    free(h_set);
    h_set = NULL;

}

bool can_correlate(kd_cell *point, kd_cell *cell)
{
    float sum = 0.0f;
    float boxsum = 0.0f;
    int i = 0;
    for (i = 0; i < DIM; i ++)
    {
        float center = (cell->coord_max[i] + cell->min[i]) / 2;
        float boxdist = (cell->coord_max[i] - cell->min[i]) / 2;
        float dist = point->coord_max[i] - center;
        sum += dist * dist;
        boxsum += boxdist * boxdist;
	}

	printf("%f - %f = %f\n", sqrt(sum), sqrt(boxsum), (sqrt(sum) - sqrt(boxsum)));
    if ((sqrt(sum) - sqrt(boxsum)) < RADIUS)
        return true;
    else
        return false;
}













	
