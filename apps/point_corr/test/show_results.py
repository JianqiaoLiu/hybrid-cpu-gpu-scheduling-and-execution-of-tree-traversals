#! /usr/bin/env python

import os
import sys
import pickle
import math
#import pretty_table

if len(sys.argv) < 2:
    print "usage: summarize.py table_file1 [table_file2] [table_file3] ..."
    print "   table_file is a file name of the table built by running collect.py"
    print
    sys.exit(1)

for table_file in sys.argv[1:]:

    table={}
    summary={}

    try:
        f=open(table_file, "r")
        table=pickle.load(f)
        f.close()
    except Exception as e:
        print "Could not load table from %s: %s" % (table_file,e)
        sys.exit(1)

        # summarize all of the data to:
        # count sum min max avg stddev
	
    name_length=20
    value_length=10

    for col in table:
        row = table[col]
        mean = sum([float(x) for x in row])/len(row)
	print "%20s" % (col),
	for x in row:
	    print "%15s\t" % (str(x)),
	print ("\tmean: %15.3f") % (mean)

    print

sys.exit(0)
