#ifndef __SPLICE_H_
#define __SPLICE_H_

#include "common.h"
#include "data_types.h"

class SpliceNode {
public:
	SpliceNode();
	~SpliceNode();
    
	void add(kd_cell *point) { points.push_back(point); }
	int size() { return points.size(); }
	void clear() { points.clear(); }
    
	vector<kd_cell *> points;
};

class SpliceSet {
public:
	SpliceSet();
	~SpliceSet();
    
	void map_tree(int depth, int index, kd_cell *node);
	SpliceNode* get_node(int i) {
		assert(i < num_nodes);
		return splice_nodes_dest[i];
	}
	void do_splice();

    static int int_exp(int n);
	static void init(int d);
	static int get_depth_param() { return depth_param; }
    
private:
	void swap_nodes(int i);
	void phase_infix(int depth1, int phase1, int depth2, int phase2);
	void top_phase(int depth, int phase);
	void bottom_phase(const int start_node_index);
//	void do_bottom_phase(kd_cell *point, int target_base, SpliceNode *sn);
    void do_bottom_phase(SpliceNode *sn, int target_base);
	void process_top_phase(kd_cell *point, kd_cell *node, int depth);
//	void process_bottom_phase(kd_cell *point, kd_cell *node);
    void process_bottom_phase(SpliceNode *sn, int node_index);

public:
	SpliceNode **splice_nodes_src;
	SpliceNode **splice_nodes_dest;
	kd_cell **tree_map;
    
	static int depth_param;
	static int depth_param_half;
	static int num_leaf_nodes;
	static int num_nodes;
};



#endif
