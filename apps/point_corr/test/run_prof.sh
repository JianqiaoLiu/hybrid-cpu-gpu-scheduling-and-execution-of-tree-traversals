#! /bin/bash -x
input=../input/covtype.7d
#f=$(basename $input)
rm -rf result*

dim=7
npoints=500000
track_traversals=1
rad=0.32f
max_depth=$
splice_depth=7
#iter_times=3

#for((i = dim; i < max_depth; i ++))
#do
#	for((j = 0; j < iter_times; j ++))
#	do
		make clean
#		make DIM=$dim TRACK_TRAVERSALS=$track_traversals RADIUS=$rad SPLICE_DEPTH=$splice_depth
        make DIM=$dim RADIUS=$rad SPLICE_DEPTH=$splice_depth
#		./point_corr $npoints ${input} 2>&1 | collect.py result_${i}.stats 2>&1 >> result_${i}.out
		./point_corr $npoints ${input} 2>&1
#	done
#done
#for((i = dim; i < max_depth; i ++))
#do
#	printf "DIM: %d\tDepth: %d\n" $dim $i >> summary_DIM_${dim}.out
#	python show_results.py result_${i}.stats 2>&1 >> summary_DIM_${dim}.out
#done

