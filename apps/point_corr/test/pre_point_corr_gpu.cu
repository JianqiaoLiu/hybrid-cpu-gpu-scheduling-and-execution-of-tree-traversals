#include "pre_point_corr_gpu.h"

gpu_tree* build_pre_gpu_tree(kd_cell *c_node)
{
    gpu_tree *pre_root;
    SAFE_MALLOC(pre_root, sizeof(gpu_tree));

    int index = 1;
    pre_root->tree_depth = SPLICE_DEPTH;
    pre_root->max_nnodes = 2;
    int n = SPLICE_DEPTH;
    while(n > 0)
    {
        pre_root->max_nnodes *= 2;
        n --;
    }
    
    // allocate the pre tree
    SAFE_MALLOC(pre_root->nodes0, sizeof(gpu_node0) * pre_root->max_nnodes);
    SAFE_MALLOC(pre_root->nodes1, sizeof(gpu_node1) * pre_root->max_nnodes);
    SAFE_MALLOC(pre_root->nodes2, sizeof(gpu_node2) * pre_root->max_nnodes);
    SAFE_MALLOC(pre_root->nodes3, sizeof(gpu_node3) * pre_root->max_nnodes);

    block_pre_gpu_tree(c_node, pre_root, index, 0);
    return pre_root;    
}

int block_pre_gpu_tree(kd_cell* c_node, gpu_tree* pre_root, int index, int depth)
{
    if(depth > SPLICE_DEPTH)
        return -1;
 
    int my_index = index;
    pre_root->nodes3[my_index].corr = 0;
    pre_root->nodes1[my_index].splitType = c_node->splitType;
    pre_root->nodes1[my_index].my_index = index;
    for (int i = 0; i < DIM; i ++)
    {
        pre_root->nodes0[my_index].coord_max[i] = c_node->coord_max[i];
        pre_root->nodes0[my_index].min[i] = c_node->min[i];
    }
//    pre_root->nodes3[my_index].cpu_addr = NULL;
    pre_root->nodes3[my_index].point_id = -1;

    if(c_node->left != NULL)
    {
        pre_root->nodes2[my_index].left = block_pre_gpu_tree(c_node->left, pre_root, 2 * index, depth + 1);;
    }
    else
    {
        pre_root->nodes2[my_index].left = -1;
    }
    
	if(c_node->right != NULL)
    {
        pre_root->nodes2[my_index].right = block_pre_gpu_tree(c_node->right, pre_root, 2 * index + 1, depth + 1);
    }
    else
    {
        pre_root->nodes2[my_index].right = -1;;
    }

    return my_index;
}

void free_pre_gpu_tree(gpu_tree *pre_root)
{
    free(pre_root->nodes0);
    free(pre_root->nodes1);
    free(pre_root->nodes2);
    free(pre_root->nodes3);
    free(pre_root);
}


__global__ void pre_compute_correlation(pc_pre_kernel_params params)
{
#ifdef USE_SMEM
    __shared__ float p_coord[DIM][THREADS_PER_BLOCK];
    __shared__ gpu_node0 cur_node0[NWARPS_PER_BLOCK];
    __shared__ gpu_node1 cur_node1[NWARPS_PER_BLOCK];
    __shared__ gpu_node2 cur_node2[NWARPS_PER_BLOCK];
    __shared__ int stack[NWARPS_PER_BLOCK][128];
    //__shared__ int mask[NWARPS_PER_BLOCK][128];
#else
    float p_coord[DIM][THREADS_PER_BLOCK];
    gpu_node0 cur_node0;
    gpu_node1 cur_node1;
    gpu_node2 cur_node2;
    int stack[128];
    //unsigned int mask[128];
#endif

    int sp = 0;
    int cur_node_index = 0;
    int i = 0;
    int j = 0;
    int can_correlate_result = 0;
    int p_corr = 0;
    //unsigned int mask[128];

    float rad = 0.0;
    float dist = 0.0;
    float sum = 0.0;
    float boxsum = 0.0;
    float boxdist = 0.0;
    float center = 0.0;

#ifdef TRACK_TRAVERSALS
    int p_nodes_accessed = 0;
    int p_nodes_needed = 0;
#endif

    rad = params.rad;
    for (i = blockIdx.x*blockDim.x+threadIdx.x; i < params.npoints; i += gridDim.x*blockDim.x)
    {
        p_corr = params.set.nodes3[i].corr;
        for (j = 0; j < DIM; j ++)
        {
            p_coord[j][threadIdx.x] = params.set.nodes0[i].coord_max[j];
        }

        STACK_INIT();
        stack[1] = params.root_index;

        while (sp >= 1)
        {
            cur_node_index = STACK_TOP_NODE_INDEX;
            //cur_mask = STACK_TOP_MASK;
            CUR_NODE0 = params.tree.nodes0[cur_node_index];

#ifdef TRACK_TRAVERSALS
            p_nodes_accessed ++;
#endif

            STACK_POP();

            // inline call: can_correlate(...)
            sum = 0.0;
            boxsum = 0.0;
            for (j = 0; j < DIM; j ++)
            {
                center = (CUR_NODE0.coord_max[j] + CUR_NODE0.min[j]) / 2;
                boxdist = (CUR_NODE0.coord_max[j] - CUR_NODE0.min[j]) / 2;
                dist = p_coord[j][threadIdx.x] - center;
                sum += dist * dist;
                boxsum += boxdist * boxdist;
            }

            if (sqrt(sum) - sqrt(boxsum) < rad)
            {
                can_correlate_result = 1; // && (cur_mask & (1 << threadIdx.x % 32) != 0)
#ifdef TRACK_TRAVERSALS
				p_nodes_needed ++;
#endif				
            	params.relation_matrix[(cur_node_index - 1) * params.npoints + params.set.nodes3[i].point_id] = can_correlate_result;

				CUR_NODE1 = params.tree.nodes1[cur_node_index];
				if (CUR_NODE1.splitType == SPLIT_LEAF)
				{
                    dist = 0.0;
                    for (j = 0; j < DIM; j ++)
                    {
                        dist += (p_coord[j][threadIdx.x] - CUR_NODE0.coord_max[j]) * (p_coord[j][threadIdx.x] - CUR_NODE0.coord_max[j]);
                    }

                    dist = sqrt(dist);
                    if (dist < rad)
                    {
                        p_corr ++; // =(100 * block_node_coord[0][WARP_INDEX][k]);
                    }
				}
				else
				{
                    CUR_NODE2 = params.tree.nodes2[cur_node_index];
                    // push children
                    if (CUR_NODE2.right != -1)
                    {
                        STACK_PUSH();
                        STACK_TOP_NODE_INDEX = CUR_NODE2.right;
                        // STACK_TOP_MASK = __ballot(can_correlate_result);
                    }

                    if (CUR_NODE2.left != -1)
                    {
                        STACK_PUSH();
                        STACK_TOP_NODE_INDEX = CUR_NODE2.left;
                        // STACK_TOP_MASK = __ballot(can_correlate_result);
                    }
                }
            }
        }

		params.set.nodes3[i].corr = p_corr;
#ifdef TRACK_TRAVERSALS
        params.set.nodes0[i].nodes_accessed = p_nodes_accessed;
        params.set.nodes0[i].nodes_needed = p_nodes_needed;
#endif		
    }
}







