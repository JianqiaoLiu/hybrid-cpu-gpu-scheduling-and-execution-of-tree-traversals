#ifndef __POINT_CORR_H_
#define __POINT_CORR_H_

void read_input(int argc, char *argv[]);
kd_cell * build_tree(kd_cell ** points, int split, int lb, int ui, int index);
int kdnode_cmp(const void *a, const void *b);
void print_tree(kd_cell * root, int depth);
void free_tree(kd_cell *root);

void find_correlation(int start, int end);
void correlation_search(SpliceNode *sn, int root_index);
void correlation_search_map_tree(kd_cell *node, int _depth, int _index, SpliceSet *_set);
void correlation_search_splice(kd_cell *point, kd_cell *node, int _depth, SpliceSet *_set);
bool can_correlate(kd_cell *point, kd_cell *cell);

static inline float distance_axis(kd_cell *a, kd_cell *b, int axis);
static inline float distance(kd_cell *a, kd_cell *b);

#endif
