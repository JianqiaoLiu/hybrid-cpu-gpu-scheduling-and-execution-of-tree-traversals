#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include <float.h>
#include <math.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include "../../../common/util_common.h"

#include "pc.h"

unsigned int nthreads;
unsigned int npoints;
unsigned long long corr_sum;

#ifdef TRACK_TRAVERSALS
unsigned long long sum_nodes_accessed;
#endif

struct kdnode ** points;
struct kdnode * root;

static long leaves = 0;
static long non_leaves = 0;
static int test = 0;
static int complete = 0;

TIME_INIT(overall);
TIME_INIT(read_input);
TIME_INIT(build_tree);
TIME_INIT(traverse);

unsigned int sort_flag, verbose_flag, check_flag;

int sort_split; /* axis component compared */
int sortidx;

void* call_compute_corr(void *args) {
	int i;
	struct thread_args *targs = (struct thread_args*)args;
	for(i = targs->lb; i < targs->ub; i++) {
        test = 0;
        complete = 0;
		compute_correlation(points[i], root, RADIUS);
        printf("point id = %d, max[0] = %f, test = %d, complete = %d\n", points[i]->id, points[i]->coord_max[0], test, complete);
	}

	pthread_exit(NULL);
}

int main(int argc, char ** argv) {
	unsigned long long i;

	TIME_START(overall);
	TIME_START(read_input);

	read_input(argc, argv);

	TIME_END(read_input);
	TIME_START(build_tree);

	printf("configuration: nthreads = %d, sort_flag = %d, verbose_flag=%d, check_flag=%d, npoints=%d, radius=%f\n", nthreads, sort_flag, verbose_flag, check_flag, npoints, RADIUS);

	sortidx=0;
	root = build_tree(points, 0, 0, npoints-1);

	if(!sort_flag) {
		for(i = 0; i < npoints; i++) {
			struct kdnode *tmp = points[i];
			int idx = rand() % npoints;
			points[i] = points[idx];
			points[idx] = tmp;
		}
	}

	TIME_END(build_tree);
	TIME_START(traverse);

	struct thread_args *args;
	pthread_t *threads;

	SAFE_MALLOC(args, sizeof(struct thread_args)*nthreads);
	SAFE_MALLOC(threads, sizeof(pthread_t)*nthreads);

	int start = 0;
	for(i = 0; i < nthreads; i++) {
		int num = (npoints - start) / (nthreads - i);
		args[i].tid = i;
		args[i].lb = start;
		args[i].ub = start + num;
		start += num;
		//printf("%d %d\n", args[i].lb, args[i].ub);
	}
	
	for(i = 0; i < nthreads; i++) {
		if(pthread_create(&threads[i], NULL, call_compute_corr, &args[i]) != 0) {
			fprintf(stderr, "Could not create thread %d\n", i);
			exit(1);
		}
	}

	for(i = 0; i < nthreads; i++) {
		pthread_join(threads[i], NULL);
	}
	
	free(args);
	free(threads);
    
    printf("Leaves = %d\n", leaves);
    printf("Non leaves = %d\n", non_leaves);
	
	TIME_END(traverse);

	corr_sum = 0;
	for(i = 0; i < npoints; i++) {
		corr_sum += (unsigned long long)points[i]->corr;
		
		#ifdef TRACK_TRAVERSALS
		sum_nodes_accessed += (unsigned long long)points[i]->nodes_accessed;
		#endif
	}

	printf("avg corr: %f\n", (float)corr_sum / npoints);

	#ifdef TRACK_TRAVERSALS
	printf("num nodes: %llu\n", sum_nodes_accessed);
	printf("avg nodes: %llu\n", sum_nodes_accessed / npoints);
	#endif


	TIME_END(overall);

	TIME_ELAPSED_PRINT(overall, stdout);
	TIME_ELAPSED_PRINT(read_input, stdout);
	TIME_ELAPSED_PRINT(build_tree, stdout);
	TIME_ELAPSED_PRINT(traverse, stdout);

	return 0;
}

void compute_correlation(struct kdnode * point, struct kdnode * root, float rad) {

	#ifdef TRACK_TRAVERSALS
	point->nodes_accessed++;
	#endif
    
    test ++;
	// termination condition
	if(!can_correlate(point, root, rad)) {
/*        if (point->id == 0) {
            
            if (root->splitType == SPLIT_LEAF) {
                leaves ++;
            } else {
                non_leaves ++;
            }
        }*/
		return;
	}

	if(root->splitType == SPLIT_LEAF) {
		if(in_radii(point, root, rad)) {
			point->corr++;
		}
	} else {
		if(root->left)
			compute_correlation(point, root->left, rad);

		if(root->right)
			compute_correlation(point, root->right, rad);
	}
    complete ++;
}

int in_radii(struct kdnode * point, struct kdnode * cell, float rad) {
	float dist=0.0;
	int i;

	for(i = 0; i < DIM; i++) {
		dist += (point->coord_max[i] - cell->coord_max[i])*(point->coord_max[i] - cell->coord_max[i]);
	}

	if(sqrt(dist) < rad) {
		return 1;
	} else {
		return 0;
	}
}


int can_correlate(struct kdnode * point, struct kdnode * cell, float rad) {
	float dist=0.0;
	float boxdist=0.0;
	float sum=0.0;
	float boxsum=0.0;
	float center=0.0;
	int i;

	for(i = 0; i < DIM; i++) {
		center = (cell->coord_max[i] + cell->min[i]) / 2;
		boxdist = (cell->coord_max[i] - cell->min[i]) / 2;
		dist = point->coord_max[i] - center;
		sum += dist * dist;
		boxsum += boxdist * boxdist;
	}

	if(sqrt(sum) - sqrt(boxsum) < rad)
		return 1;
	else
		return 0;
}


struct kdnode * build_tree(struct kdnode ** points, int split, int lb, int ub) {
	int mid;
	int j;
	struct kdnode *node;

	if(lb > ub)
		return 0;

	if(lb == ub) {
		return points[lb];
	} else {

		sort_split = split;
		qsort(&points[lb], ub - lb + 1, sizeof(struct kdnode*), kdnode_cmp);
		mid = (ub + lb) / 2;

		// create a new node to contains the points:
		SAFE_MALLOC(node, sizeof(struct kdnode));
		node->splitType = split;

		node->left = build_tree(points, (split+1) % DIM, lb, mid);
		node->right = build_tree(points, (split+1) % DIM, mid+1, ub);

		node->corr = 0;

		for(j = 0; j < DIM; j++) {
			node->min[j] = FLT_MAX;
			node->coord_max[j] = FLT_MIN; 
		}

		if(node->left != NULL) {
			for(j = 0; j < DIM; j++) {

				if(node->coord_max[j] < node->left->coord_max[j]) {
					node->coord_max[j] = node->left->coord_max[j];
				}

				if(node->min[j] > node->left->min[j]) {
					node->min[j] = node->left->min[j];
				}
			}
		}

		if(node->right != NULL) {
			for(j = 0; j < DIM; j++) {
				if(node->coord_max[j] < node->right->coord_max[j]) {
					node->coord_max[j] = node->right->coord_max[j];
				}

				if(node->min[j] > node->right->min[j]) {
					node->min[j] = node->right->min[j];
				}
			}
		}

		return node;
	}	
}

int kdnode_cmp(const void *a, const void *b) {
	/* note: sort split must be updated before call to qsort */
	struct kdnode **na = (struct kdnode**)a;
	struct kdnode **nb = (struct kdnode**)b;

	if((*na)->coord_max[sort_split] < (*nb)->coord_max[sort_split]) {
		return -1;
	} else if((*na)->coord_max[sort_split] > (*nb)->coord_max[sort_split]) {
		return 1;
	} else {
		return 0;
	}
}

void read_input(int argc, char * argv[]) {
	FILE * in = stdin;
	int i, j, c, label, junk;

	if(argc <= 2) {
		fprintf(stderr, "Usage: pc <infile> <npoints>\n");
		exit(1);
	}

	check_flag = 0;
	sort_flag = 0;
	verbose_flag = 0;
	nthreads = 1;
	i=0;
	while((c = getopt(argc, argv, "cvt:s")) != -1) {
		switch(c) {
		case 'c':
			check_flag = 1;
			i++;
			break;

		case 'v':
			verbose_flag = 1;
			i++;
			break;

		case 't':
			nthreads = atoi(optarg);
			if(nthreads <= 0) {
				fprintf(stderr, "Error: invalid number of threads.\n");
				exit(1);
			}
			i+=2;
			break;

		case 's':
			sort_flag = 1;
			i++;
			break;

		case '?':
			fprintf(stderr, "Error: unknown option.\n");
			exit(1);
			break;

		default:
			abort();
		}
	}
	
	npoints = 0;
	for(i = optind; i < argc; i++) {
		switch(i - optind) {
		case 0:
			in = fopen(argv[i], "r");
			if(!in) {
				fprintf(stderr, "Error: could not open %s for reading.\n", argv[i]);
				exit(1);
			}
			break;

		case 1:
			npoints = atoi(argv[i]);
			if(npoints <= 0) {
				fprintf(stderr, "Error: invalid number of points.\n");
				exit(1);
			}
			break;
		}		
	}
	
	// initialize the points	
	SAFE_MALLOC(points, sizeof(struct kdnode*)*npoints);

	for(i=0; i<npoints; i++) {
		SAFE_MALLOC(points[i], sizeof(struct kdnode));
		points[i]->splitType = SPLIT_LEAF;
		points[i]->left = 0;
		points[i]->right = 0;
		points[i]->corr = 0;	 
		points[i]->id = i;
		#ifdef TRACK_TRAVERSALS
		points[i]->nodes_accessed = 0;
		#endif

		// read coord from input
		fscanf(in, "%d", &label);
		for(j = 0; j < DIM; j++) {
			if(fscanf(in, "%f", &(points[i]->coord_max[j])) != 1) {
				fprintf(stderr, "Error: Invalid point %d\n", i);
				exit(1);
			}

			points[i]->min[j] = points[i]->coord_max[j];
		}
	}

	if(in != stdin) {
		fclose(in);
	}
}
