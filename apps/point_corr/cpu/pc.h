#ifndef __PC_H_
#define __PC_H_

#ifndef DIM
#define DIM 7
#endif

#ifndef RADIUS
#define RADIUS (0.32f)
#endif

#define SPLIT_LEAF (DIM)

struct kdnode {
	unsigned long long corr;

	int splitType;

	float coord_max[DIM];
	float min[DIM];

	struct kdnode *left;
	struct kdnode *right;
	int id;
	
	#ifdef TRACK_TRAVERSALS
	int nodes_accessed;
	#endif

};

struct thread_args {
	int tid;
	int lb;
	int ub;	
};



int main(int argc, char ** argv);
void compute_correlation(struct kdnode * point, struct kdnode * root, float rad);
int can_correlate(struct kdnode * point, struct kdnode * cell, float rad);
int in_radii(struct kdnode * point, struct kdnode * cell, float rad);

void* call_compute_corr(void *args);

void read_input(int argc, char *argv[]);
struct kdnode * build_tree(struct kdnode ** points, int split, int lb, int ub);
int kdnode_cmp(const void *a, const void *b);
int subdivide(struct kdnode * points, int split, int lb, int ub);
void print_tree(struct kdnode * root, int depth);

#endif

