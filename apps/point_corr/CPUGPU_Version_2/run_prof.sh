#! /bin/bash -x
input=../../../input/pc/random.7d
#f=$(basename $input)
rm -rf result*

dim=7
npoints=200000
track_traversals=1
rad=0.32f
splice_depth=8
iter_times=1

#for((i = dim; i < max_depth; i ++))
#do
	for((j = 0; j < iter_times; j ++))
	do
		make clean
		make DIM=$dim TRACK_TRAVERSALS=$track_traversals RADIUS=$rad SPLICE_DEPTH=$splice_depth
#		make DIM=$dim RADIUS=$rad SPLICE_DEPTH=$splice_dept
#		./point_corr $npoints ${input} 2>&1 | collect.py result_${i}.stats 2>&1 >> result_${i}.out
		./point_corr -v ${input} $npoints 2>&1
	done
#done

