#ifndef __POINT_CORR_DATA_TYPES_H_
#define __POINT_CORR_DATA_TYPES_H_

#include "util_common.h"
//#ifndef TRACK_TRAVERSALS
//#define TRACK_TRAVERSALS
//#endif

#ifndef DIM
#define DIM 7
#endif

#ifndef RADIUS
#define RADIUS (0.32f)
#endif

#ifndef SPLICE_DEPTH
#define SPLICE_DEPTH 2
#endif

#define SPLIT_LEAF (DIM)

typedef struct _kd_cell
{
	int id;
	int corr;
	int splitType;
	int depth;
	int pre_id;
	float coord_max[DIM];
	float min[DIM];
	struct _kd_cell *left;
	struct _kd_cell *right;

	#ifdef TRACK_TRAVERSALS
	int nodes_accessed;
	int nodes_needed;
	#endif
} kd_cell;

typedef struct _gpu_node0
{
	#ifdef TRACK_TRAVERSALS
	int nodes_accessed;
	int nodes_needed;
	#endif
	
	float coord_max[DIM];
	float min[DIM];
} gpu_node0;

typedef struct _gpu_node1
{
	int splitType;
    int my_index;
	int depth;
	int pre_id;
} gpu_node1;

typedef struct _gpu_node2
{
	int left;
	int right;
} gpu_node2;

typedef struct _gpu_node3
{
	int corr;
	int point_id;
	//	kd_cell *cpu_addr;
} gpu_node3;

typedef struct _gpu_tree
{
	gpu_node0 *nodes0;
	gpu_node1 *nodes1;
	gpu_node2 *nodes2;
	gpu_node3 *nodes3;

	unsigned int nnodes;
	unsigned int max_nnodes;
	unsigned int tree_depth;
} gpu_tree;

typedef struct _gpu_point_set
{
    unsigned int npoints;
	gpu_node0 *nodes0;
	gpu_node3 *nodes3;
} gpu_point_set;

typedef struct _pc_kernel_params {
	gpu_tree tree;
	int root_index;
    gpu_point_set set;
	float rad;
	int npoints;
	int* index_buffer;
} pc_kernel_params;

typedef struct _pc_pre_kernel_params {
    gpu_tree tree;
    int root_index;
    gpu_point_set set;
    bool *relation_matrix;
    float rad;
    int npoints;
    int tree_max_nnodes;
} pc_pre_kernel_params;

/* Cuda Macros */
#define THREADS_PER_WARP 32

#define NUM_THREAD_BLOCKS (14*8)
#define THREADS_PER_BLOCK (THREADS_PER_WARP * 8)

#define NWARPS_PER_BLOCK (THREADS_PER_BLOCK / THREADS_PER_WARP)
#define NWARPS (NUM_THREAD_BLOCKS*NWARPS_PER_BLOCK)

#define WARP_INDEX (threadIdx.x >> 5)
#define GLOBAL_WARP_INDEX (WARP_INDEX + (blockIdx.x*NWARPS_PER_BLOCK))
#define THREAD_INDEX_IN_WARP threadIdx.x & 0x1f

/* Kernel Macros */

#ifdef USE_SMEM
#define STACK_INIT() sp = 1; stack[WARP_INDEX][1] = 0
//mask[WARP_INDEX][1] = 0xffffffff
#define STACK_POP() sp -= 1
#define STACK_PUSH() sp += 1
#define STACK_TOP_NODE_INDEX stack[WARP_INDEX][sp]
#define STACK_TOP_MASK mask[WARP_INDEX][sp]
#define CUR_NODE0 cur_node0[WARP_INDEX]
#define CUR_NODE1 cur_node1[WARP_INDEX]
#define CUR_NODE2 cur_node2[WARP_INDEX]
#else
#define STACK_INIT() sp = 1; stack[1] = 0
//mask[1] = 0xffffffff
#define STACK_POP() sp -= 1
#define STACK_PUSH() sp += 1
#define STACK_TOP_NODE_INDEX stack[sp]
#define STACK_TOP_MASK mask[sp]
#define CUR_NODE0 cur_node0
#define CUR_NODE1 cur_node1
#define CUR_NODE2 cur_node2
#endif

#endif
