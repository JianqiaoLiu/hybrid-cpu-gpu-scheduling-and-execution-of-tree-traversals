#ifndef __POINT_CORR_H_
#define __POINT_CORR_H_

#include "util_common.h"
#include "point_corr_data_types.h"
#include "point_corr_pre_kernel.h"
#include "point_corr_kernel.h"
#include "point_corr_mem.h"
#include "point_corr_functions.h"
#include <list>

void read_input(int argc, char *argv[]);
kd_cell * build_tree(kd_cell ** points, int split, int lb, int ui, int index);
int kdnode_cmp(const void *a, const void *b);
void print_tree(kd_cell * root, int depth);
void free_tree(kd_cell *root);

void find_correlation(int start, int end);
bool can_correlate(kd_cell *point, kd_cell *cell);

static inline float distance_axis(kd_cell *a, kd_cell *b, int axis);
static inline float distance(kd_cell *a, kd_cell *b);

#endif
