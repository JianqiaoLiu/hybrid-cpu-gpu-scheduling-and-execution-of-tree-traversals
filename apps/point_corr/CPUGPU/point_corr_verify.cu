//#define OVERLAPPING

#include "point_corr.h"

int npoints = 0; // number of input points
kd_cell **points = NULL; // input points
kd_cell *root = NULL; // root of the tree

int sort_flag = 0;
int verbose_flag = 0;
int check_flag = 0;
int nthreads = 0;

unsigned long corr_sum = 0;
unsigned int sum_of_nodes = 0;

#ifdef TRACK_TRAVERSALS
unsigned long long nodes_accessed_sum = 0;
unsigned long long nodes_needed_sum = 0;
#endif

gpu_tree *h_root = NULL; // root of the host GPU tree
gpu_tree d_root;

gpu_tree *h_pre_root = NULL; // root of the pre GPU tree
gpu_tree d_pre_root;

int *h_correlation_matrix;
int *d_correlation_matrix;
unsigned int COLS = 0;
unsigned int ROWS = 0;

int sort_split = 0; // axis component compared
int sortidx = 0;

cudaError_t e;
dim3 tpb(THREADS_PER_BLOCK);
dim3 nb(NUM_THREAD_BLOCKS);

TIME_INIT(overall);
TIME_INIT(read_input);
TIME_INIT(build_tree);
TIME_INIT(init_kernel);
TIME_INIT(find_correlation);
TIME_INIT(gpu_kernel);

TIME_INIT(pre_tree);
TIME_INIT(pre_kernel);
TIME_INIT(pre_all);

int main(int argc, char* argv[]) 
{
#ifdef TRACK_TRAVERSALS
    cudaDeviceProp prop;
//    cudaGetDeviceProperties(&prop, 0);
//    printf("Device : %s. The splice depth is %d.\n", prop.name, SPLICE_DEPTH);
#endif
	int i = 0; // loop variable
	int j = 0; // loop variable

	srand(0); // for quicksort

	TIME_START(overall);

    TIME_START(init_kernel);
    init_kernel<<<1, 1>>>();
    TIME_END(init_kernel);

	TIME_START(read_input);
	read_input(argc, argv);
	TIME_END(read_input);
	
	TIME_START(build_tree);
	// cpu tree
	root = build_tree(points, 0, 0, npoints - 1, 1);
	// gpu tree
    h_root = build_gpu_tree(root); // build up the gpu tree
    d_root.nnodes = h_root->nnodes;
	d_root.tree_depth = h_root->tree_depth;
    alloc_tree_dev(h_root, &d_root);
    copy_tree_to_dev(h_root, &d_root);
	TIME_END(build_tree);

	if (!sort_flag)
	{
		srand(0);
		for (i = 0; i < npoints; i ++)
		{
			j = rand() % npoints;
			kd_cell *temp = points[i];
			points[i] = points[j];
			points[j] = temp;
		}
	}

// Pre compute correlation matrix	
	TIME_START(pre_all);
	TIME_START(pre_tree);
	h_pre_root = build_pre_gpu_tree(root); // build up the pre gpu tree
    alloc_tree_dev(h_pre_root, &d_pre_root);
	copy_tree_to_dev(h_pre_root, &d_pre_root);
	TIME_END(pre_tree);

    ROWS = npoints;
    COLS = h_pre_root->max_nnodes;
    int nMatrixSize = ROWS * COLS;
	SAFE_MALLOC(h_correlation_matrix, sizeof(int) * nMatrixSize);  
    CUDA_SAFE_CALL(cudaMalloc(&(d_correlation_matrix), sizeof(int) * nMatrixSize));

	gpu_point_set *h_set = NULL; // root of the host GPU point set
	gpu_point_set d_set;
    SAFE_MALLOC(h_set, sizeof(gpu_point_set));
    h_set->npoints = npoints;
    SAFE_MALLOC(h_set->nodes0, sizeof(gpu_node0) * npoints);
    SAFE_MALLOC(h_set->nodes3, sizeof(gpu_node3) * npoints);
    for (i = 0 ; i < h_set->npoints; i++)
    {
#ifdef TRACK_TRAVERSALS
        h_set->nodes0[i].nodes_accessed = points[i]->nodes_accessed;
        h_set->nodes0[i].nodes_needed = points[i]->nodes_needed;
#endif
        for (j = 0; j < DIM; j ++)
        {
            h_set->nodes0[i].coord_max[j] = points[i]->coord_max[j];
            h_set->nodes0[i].min[j] = points[i]->min[j];
        }

        h_set->nodes3[i].corr = points[i]->corr;
//        h_set->nodes3[i].cpu_addr = NULL;
		h_set->nodes3[i].point_id = points[i]->id;
    }

    d_set.npoints = h_set->npoints;
    alloc_set_dev(h_set, &d_set);
    copy_set_to_dev(h_set, &d_set);
	
	pc_pre_kernel_params d_params;
    d_params.tree = d_pre_root;
	d_params.root_index = 1;
    d_params.set = d_set;
	d_params.relation_matrix = d_correlation_matrix;
    d_params.rad = RADIUS;
    d_params.npoints = d_set.npoints;
	d_params.tree_max_nnodes = h_pre_root->max_nnodes;
	printf("ROWS = %d, COLS = %d. tree_max_nnodes = %d.\n", ROWS, COLS, h_pre_root->max_nnodes);


    // ** Pre Kernel ** //
	TIME_START(pre_kernel);
	pre_compute_correlation<<<nb, tpb>>>(d_params);
    cudaThreadSynchronize();
	TIME_END(pre_kernel);
    e = cudaGetLastError();
    if (e != cudaSuccess)
    {
        fprintf(stderr, "error: kernel error: %s.\n", cudaGetErrorString(e));
        exit(1);
    }
    
    copy_set_to_host(h_set, &d_set);
	CUDA_SAFE_CALL(cudaMemcpy(h_correlation_matrix, d_correlation_matrix, sizeof(int) * nMatrixSize, cudaMemcpyDeviceToHost));

    CUDA_SAFE_CALL(cudaFree(d_correlation_matrix));
    d_correlation_matrix = NULL;
// we can't delete the point set now, maybe can do more work later
//  free_set_dev(&d_set);
	free_pre_gpu_tree(h_pre_root);
	free_tree_dev(&d_pre_root);
    TIME_END(pre_all);

    TIME_START(find_correlation);
    find_correlation(0, npoints);
//    find_correlation(0, 1);
    TIME_END(find_correlation);

    free(h_set->nodes0);
    free(h_set->nodes3);
    free(h_set);
    h_set = NULL;

    for(i = 0; i < npoints; i++)
	{
		corr_sum += (unsigned long)points[i]->corr;
#ifdef TRACK_TRAVERSALS
		nodes_accessed_sum += (unsigned long)points[i]->nodes_accessed;
		nodes_needed_sum += (unsigned long)points[i]->nodes_needed;
#endif
	}
    printf("@ avg_corr: %f\n", (float)corr_sum / npoints);
#ifdef TRACK_TRAVERSALS
	printf("@ sum_accessed: %ld\n", nodes_accessed_sum);
	printf("@ sum_needed: %ld\n", nodes_needed_sum);
	printf("@ ratio: %f\n", (float)nodes_needed_sum / nodes_accessed_sum);
#endif
	
    free_tree_dev(&d_root);
    free_gpu_tree(h_root);
    h_root = NULL;
	free(h_correlation_matrix);

    TIME_END(overall);
 
    TIME_ELAPSED_PRINT(overall, stdout);
    TIME_ELAPSED_PRINT(read_input, stdout);
    TIME_ELAPSED_PRINT(build_tree, stdout);
    TIME_ELAPSED_PRINT(init_kernel, stdout);    
    TIME_ELAPSED_PRINT(find_correlation, stdout);
    TIME_ELAPSED_PRINT(gpu_kernel, stdout);

	TIME_ELAPSED_PRINT(pre_tree, stdout);
	TIME_ELAPSED_PRINT(pre_kernel, stdout);
	TIME_ELAPSED_PRINT(pre_all, stdout);
}

void find_correlation(int start, int end)
{
	int bytes = npoints * sizeof(int);
	int* cpu_buffer;
	int* gpu_buffer;
	SAFE_MALLOC(cpu_buffer, bytes);
	memset(cpu_buffer, 0, bytes);
	CUDA_SAFE_CALL(cudaMalloc(&(gpu_buffer), bytes));

	int nnodes = COLS / 2;
	vector<int>* clusters;
	clusters = new vector<int> [nnodes];
	for(int i = 0; i < nnodes; i ++)
	{
		clusters[i].clear();
	}

	int index = 0;
	int rem = 0;
	for(int point = 0; point < npoints; point ++)
	{
		index = point * nnodes;
		for(int node = 0; node < nnodes; node ++)
		{
			rem = h_correlation_matrix[index] - nnodes;
			if (rem >= 0)
			{
				clusters[rem].push_back(point);
			}
			index ++;
		}
	}

	int buffer_index = 0;
	vector<int>::iterator cluster_iter;
	for(int node = 0; node < nnodes; node ++)
	{
		gpu_point_set *h_set = NULL; // root of the host GPU point set
		SAFE_MALLOC(h_set, sizeof(gpu_point_set));
	    h_set->npoints = clusters[node].size();
//		printf("step: %d, npoint: %d.\n", node+nnodes, h_set->npoints);
		SAFE_MALLOC(h_set->nodes0, sizeof(gpu_node0) * h_set->npoints);
		SAFE_MALLOC(h_set->nodes3, sizeof(gpu_node3) * h_set->npoints);

		int i = 0;
		for(cluster_iter = clusters[node].begin(); cluster_iter != clusters[node].end(); cluster_iter ++)
		{
#ifdef TRACK_TRAVERSALS
			h_set->nodes0[i].nodes_accessed = points[*cluster_iter]->nodes_accessed;
			h_set->nodes0[i].nodes_needed = points[*cluster_iter]->nodes_needed;
#endif
			for	(int j = 0; j < DIM; j ++)
			{
				h_set->nodes0[i].coord_max[j] = points[*cluster_iter]->coord_max[j];
				h_set->nodes0[i].min[j] = points[*cluster_iter]->min[j];
			}
			
			h_set->nodes3[i].corr = points[*cluster_iter]->corr;
//			h_set->nodes3[i].cpu_addr = NULL;
			h_set->nodes3[i].point_id = points[*cluster_iter]->id;
			i ++;
		}

		gpu_point_set d_set;
		d_set.npoints = h_set->npoints;
		alloc_set_dev(h_set, &d_set);
		copy_set_to_dev(h_set, &d_set);

		pc_kernel_params d_params;
		d_params.tree = d_root;
		d_params.root_index = node + nnodes;
		d_params.set = d_set;
		d_params.rad = RADIUS;
		d_params.npoints = d_set.npoints;

		// ** Kernel ** //
		TIME_RESTART(gpu_kernel);
		compute_correlation<<<nb, tpb>>>(d_params);
		cudaThreadSynchronize();
		TIME_END(gpu_kernel);
		e = cudaGetLastError();
		if (e != cudaSuccess)
		{
			fprintf(stderr, "error: kernel error: %s.\n", cudaGetErrorString(e));
			exit(1);
		}
    
		// Kernel clean up
		copy_set_to_host(h_set, &d_set);
		free_set_dev(&d_set);

		// Copy back
		i = 0;
		for(cluster_iter = clusters[node].begin(); cluster_iter != clusters[node].end(); cluster_iter ++)
		{
		#ifdef TRACK_TRAVERSALS
			points[*cluster_iter]->nodes_accessed = h_set->nodes0[i].nodes_accessed;
			points[*cluster_iter]->nodes_needed = h_set->nodes0[i].nodes_needed;
		#endif
			points[*cluster_iter]->corr = h_set->nodes3[i].corr;
			i ++;
		}

		free(h_set->nodes0);
		free(h_set->nodes3);
		free(h_set);
		h_set = NULL;
	}


}






	
