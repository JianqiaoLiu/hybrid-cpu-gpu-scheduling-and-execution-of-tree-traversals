/* -*- mode: c -*- */

#include "pc_gpu.h"
#include "pc_kernel.h"
#include "pc_kernel_mem.h"

#define PRINT
#define PRINT_0

__global__ void init_kernel(void) {
	
}

__global__ void compute_correlation(pc_kernel_params params) {
	
	float rad;
	int pidx;
	int cur_node_index;

	#ifdef USE_SMEM
	__shared__ float p_coord[DIM][THREADS_PER_BLOCK];
	__shared__ gpu_node0 cur_node0[NWARPS_PER_BLOCK];
	__shared__ gpu_node1 cur_node1[NWARPS_PER_BLOCK];
	__shared__ gpu_node2 cur_node2[NWARPS_PER_BLOCK];
	__shared__ int stack[NWARPS_PER_BLOCK][128];
  //__shared__ int mask[NWARPS_PER_BLOCK][128];
	#else
	float p_coord[DIM];
	gpu_node0 cur_node0;
	gpu_node1 cur_node1;
	gpu_node2 cur_node2;
	__shared__ int SP[NWARPS_PER_BLOCK];
	__shared__ int stack[NWARPS_PER_BLOCK][128];
	//unsigned int mask[128];
	#endif

	int i, j;
	float dist, sum, boxsum, boxdist, center;
	int p_corr;
	//unsigned int cur_mask;

	bool cond, status;
    bool opt1, opt2;
	int critical;
	int leaves = 0;
	int non_leaves = 0;

	#ifdef TRACK_TRAVERSALS
	int p_nodes_accessed;
	int p_nodes_truncated;
	#endif

	rad = params.rad;
	for(i = blockIdx.x*blockDim.x + threadIdx.x; i < params.npoints; i+= gridDim.x*blockDim.x) {

#define sp SP[WARP_INDEX]
		pidx = params.points[i];
		p_corr = 0; // params.tree.nodes[pidx].corr;
		
		#ifdef TRACK_TRAVERSALS
		p_nodes_accessed = 0;
		p_nodes_truncated = 0;
		#endif

		for(j = 0; j < DIM; j++) {
			p_coord[j] = params.tree.nodes0[pidx].coord_max[j];
		}

		STACK_INIT();
		status = 1;
		critical = 63;
		cond = 1;

		while(sp >= 1) {

			cur_node_index = STACK_TOP_NODE_INDEX;
			//cur_mask = STACK_TOP_MASK;
			CUR_NODE0 = params.tree.nodes0[cur_node_index];
			
			#ifdef TRACK_TRAVERSALS
			p_nodes_accessed++;
			#endif

            if (i == 0) {
				printf("id = %d, node = %d, sp = %d, [%d, %d, %d, %d, %d, %d, %d]，critical = %d, status = %d, ", i, cur_node_index, sp, 	stack[WARP_INDEX][0],
																																	stack[WARP_INDEX][1],
																																	stack[WARP_INDEX][2],
																																	stack[WARP_INDEX][3],
																																	stack[WARP_INDEX][4],
																																	stack[WARP_INDEX][5],
																																	stack[WARP_INDEX][6],
																																	critical, status);
			}

			if (status == 0 && critical >= sp) {
				status = 1;
				critical = 63;
			}

			if (i == 0 && status == 0) {
				printf("SUCCESS!\n");
			}

			stack[WARP_INDEX][sp] = 0;
			STACK_POP();
			
			if (status) {
				// inline call: can_correlate(...)
				sum = 0.0;
				boxsum = 0.0;
				for(j = 0; j < DIM; j++) {
					center = (CUR_NODE0.coord_max[j] + CUR_NODE0.min[j]) / 2;
					boxdist = (CUR_NODE0.coord_max[j] - CUR_NODE0.min[j]) / 2;
					dist = p_coord[j] - center;
					sum += dist * dist;
					boxsum += boxdist * boxdist;
				}

				cond = sqrt(sum) - sqrt(boxsum) < rad;

				if (i == 0) {
					printf("cond = %d, ", cond);
				}

				if(!__any(cond)) {
#ifdef TRACK_TRAVERSALS
					p_nodes_truncated++;
#endif
					if (i == 0) {
						if (params.tree.nodes1[cur_node_index].splitType == SPLIT_LEAF) {
							leaves ++;
						} else {
							non_leaves ++;
						}
						printf("skipped!\n");
					}
					continue;
				}


				unsigned int vote_work = __ballot(cond);
				unsigned int num = __popc(vote_work);
				critical = sp;
				if (!cond) {
					status = 0;
//					critical = sp;
					if (i == 0) {
						if (params.tree.nodes1[cur_node_index].splitType == SPLIT_LEAF) {
							leaves ++;
						} else {
							non_leaves ++;
							printf("we should have a zero! work threads are %d.\n", num);
						}
						printf(" ended due to cond test @ %d, critical = %d\n", cur_node_index, critical);
					}
				} else {

					if (i == 7 && cur_node_index == 6) {
						printf("\nYes!\n");
					}
					CUR_NODE1 = params.tree.nodes1[cur_node_index];
					if(CUR_NODE1.splitType == SPLIT_LEAF) {
						// inline call: in_radii(...)
						if (i == 0) {
							printf("traversal ended!\n");
						}

						dist = 0.0;
						for(j = 0; j < DIM; j++) {
							dist += (p_coord[j] - CUR_NODE0.coord_max[j]) * (p_coord[j] - CUR_NODE0.coord_max[j]);
						}
						
						dist = sqrt(dist);
						if(dist < rad) {
							p_corr++; // = (100 * block_node_coord[0][WARP_INDEX][k]);
						}
					} else {
						if (i == 0) {
							printf("to be continued! sp_1 = %d, ", sp);
						}
						CUR_NODE2 = params.tree.nodes2[cur_node_index];
						// push children
						if(CUR_NODE2.right != -1) {
							STACK_PUSH();
							STACK_TOP_NODE_INDEX = CUR_NODE2.right;
						}
				
						if(CUR_NODE2.left != -1) {
							STACK_PUSH();
							STACK_TOP_NODE_INDEX = CUR_NODE2.left;
						}

						if (i == 0) {
							printf("sp_2 = %d!\n", sp);
						}
					}
				}
			} else {
				if (i == 0) {
					printf("shut up!\n");
				}
			}
		} 
		
		
		params.tree.nodes3[pidx].corr = p_corr;
		#ifdef TRACK_TRAVERSALS
		params.tree.nodes0[pidx].nodes_accessed = p_nodes_accessed;
		params.tree.nodes0[pidx].nodes_truncated = p_nodes_truncated;
		#endif

		if (i == 0) {
		    printf("Leaves = %d\n", leaves);
		    printf("Non leaves = %d\n", non_leaves);
		}
	}
}
