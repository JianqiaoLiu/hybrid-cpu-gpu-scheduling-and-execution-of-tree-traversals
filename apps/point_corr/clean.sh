#! /bin/sh

for dir in *
do
		if [[ -d $dir ]] && [[ -f ./$dir/Makefile ]]
		then				
				cd $dir
				echo "Cleaning $dir..."
				make clean
				cd ..
		fi
done

exit 0
