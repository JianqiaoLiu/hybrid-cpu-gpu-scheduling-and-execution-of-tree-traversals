make clean;
#make DIM=7 RADIUS=0.32f SPLICE_DEPTH=8;
#./point_corr ../../../input/pc/covtype.7d 200000
#./point_corr ../../../input/pc/mnist.7d 200000
#./point_corr ../../../input/pc/random.7d 200000

make DIM=3 RADIUS=0.1f SPLICE_DEPTH=8;
./point_corr ../../../input/pc/geocity.txt 200000

make clean;
make DIM=2 RADIUS=0.1f SPLICE_DEPTH=4;
./point_corr ../../../input/pc/geocity.txt 200000

make clean;
make DIM=2 RADIUS=0.1f SPLICE_DEPTH=5;
./point_corr ../../../input/pc/geocity.txt 200000

make clean;
make DIM=2 RADIUS=0.1f SPLICE_DEPTH=6;
./point_corr ../../../input/pc/geocity.txt 200000

make clean;
make DIM=2 RADIUS=0.1f SPLICE_DEPTH=7;
./point_corr ../../../input/pc/geocity.txt 200000

make clean;
make DIM=2 RADIUS=0.1f SPLICE_DEPTH=8;
./point_corr ../../../input/pc/geocity.txt 200000

