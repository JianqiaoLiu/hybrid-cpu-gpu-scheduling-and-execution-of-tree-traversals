//
//  BBT_kenrel.hpp
//  BallTree-kNN
//
//  Created by Jianqiao Liu on 12/7/15.
//  Copyright © 2015 Jianqiao Liu. All rights reserved.
//

#ifndef __BT_PRE_KERNEL_H_
#define __BT_PRE_KERNEL_H_

#include "bt_functions.h"
#include "bt_gpu_tree.h"

extern neighbor *nearest_neighbor;

__global__ void k_nearest_neighbor_pre_search (gpu_tree gpu_tree, int nsearchpoints, datapoint *d_search_points,
											float *d_nearest_distance, int *d_nearest_point_index, int K,
											int* d_matrix, int start, int end, int interval);

#endif /* BBT_pre_kenrel_hpp */
