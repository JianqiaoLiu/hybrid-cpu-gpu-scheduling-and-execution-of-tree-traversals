//
//  BBT_kenrel.hpp
//  BallTree-kNN
//
//  Created by Jianqiao Liu on 12/7/15.
//  Copyright © 2015 Jianqiao Liu. All rights reserved.
//

#ifndef __BT_KERNEL_H_
#define __BT_KERNEL_H_

#include "bt_functions.h"
#include "bt_gpu_tree.h"

extern neighbor *nearest_neighbor;

__global__ void init_kernel(void);
//void k_nearest_neighbor_search(node* node, datapoint* point, int pos);

__global__ void k_nearest_neighbor_search (gpu_tree gpu_tree, int nsearchpoints, datapoint *d_search_points,
											float *d_nearest_distance, int *d_nearest_point_index, int K,
                                            int *index_buffer);

#endif /* BBT_kenrel_hpp */
