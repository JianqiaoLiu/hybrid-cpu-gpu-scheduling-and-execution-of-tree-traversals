//
//  BBT_kenrel.hpp
//  BallTree-kNN
//
//  Created by Jianqiao Liu on 12/7/15.
//  Copyright © 2015 Jianqiao Liu. All rights reserved.
//

#ifndef BBT_kenrel_hpp
#define BBT_kenrel_hpp

#include "functions.hpp"

extern neighbor *nearest_neighbor;

void k_nearest_neighbor_search(node* tree, datapoint* point, int idx);

#endif /* BBT_kenrel_hpp */
