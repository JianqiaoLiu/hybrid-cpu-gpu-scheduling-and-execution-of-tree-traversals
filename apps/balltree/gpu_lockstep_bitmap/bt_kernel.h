//
//  BBT_kenrel.hpp
//  BallTree-kNN
//
//  Created by Jianqiao Liu on 12/7/15.
//  Copyright © 2015 Jianqiao Liu. All rights reserved.
//

#ifndef __BT_KERNEL_H_
#define __BT_KERNEL_H_

#include "bt_functions.h"
#include "bt_gpu_tree.h"

extern neighbor *nearest_neighbor;

#define WARP_INDEX (threadIdx.x >> 5)
#define STACK_NODE stk_node[WARP_INDEX][sp]
//#define POINT cur_node2.point
#define POINT_INDEX cur_node1[WARP_INDEX].idx
//cur_node2.point[s]
#define LEFT cur_node1[WARP_INDEX].left
#define RIGHT cur_node1[WARP_INDEX].right

#define sp SP[WARP_INDEX]

#define STACK_INIT() \
	sp = 1;	

#define STACK_PUSH() sp = sp + 1; 

#define STACK_POP() sp = sp - 1; 

__global__ void init_kernel(void);
void k_nearest_neighbor_search(node* node, datapoint* point, int pos);

__global__ void k_nearest_neighbor_search (gpu_tree gpu_tree, int nsearchpoints, datapoint *d_search_points,
											float *d_nearest_distance, int *d_nearest_point_index, int K);

#endif /* BBT_kenrel_hpp */
