#! /bin/sh

for dir in *
do
		if [[ -d $dir ]] && [[ -f ./$dir/test.sh ]]
		then				
				cd $dir
				echo "***Executing $dir..."
				source test.sh
				cd ..
		fi
done

#exit 0
